import React, { Component } from 'react';

import { Grid, Row, Col, Clearfix } from 'react-bootstrap';

import EventDetails from "../containers/eventDetails";
import EventOverview from "../containers/eventOverview";
import AppCreate from "../create_event";
import Details from '../modules_create_event/details.js';
import Eventdate from '../modules_create_event/eventdate.js';
import Eventlocation from '../modules_create_event/eventlocation.js';
import People from '../modules_create_event/people.js';
import Sidebar from "./Sidebar"
import Calendar from "../containers/Calender.js";


export default class App extends Component {
  render() {
    return (
  		<div id="site-wrapper">
  		  	 <Sidebar /> 
     			 <EventOverview />
     			  <Calendar /> 
            {this.props.children}
         <AppCreate/>
   		</div>
    );
  }
}