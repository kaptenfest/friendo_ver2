import React from "react";
import { GoogleMapLoader, GoogleMap, Marker } from "react-google-maps";


export default (props) => {

	if(props.settle === ""){
		
		return (
			<GoogleMapLoader
				containerElement={ <div className="map" /> }
				googleMapElement={

			<GoogleMap 	defaultZoom={15} center={{lat: props.lat, lng: props.lng}}>

			{props.marker.map((markers, index) => (

			<Marker
				key={index}
				label={String(index+1)}
				icon="http://web.friendoapp.com/img/events/point.png"
				position={new google.maps.LatLng(markers.latitude, markers.longitude)}
				>
			</Marker>
			))}
			</GoogleMap>
				
			}
			/>
		)
	}
	
	else {

		return (
			<GoogleMapLoader
				containerElement={ <div className="map" /> }
				googleMapElement={
				
				<GoogleMap defaultZoom={15} center={{lat: Number(props.marker[0].latitude), lng: Number(props.marker[0].longitude)}}>

				<Marker
					key=""
					label=""
					icon="http://web.friendoapp.com/img/events/point_settle.png"
					position={new google.maps.LatLng(props.marker[0].latitude, props.marker[0].longitude)}
					>
				</Marker>

				</GoogleMap>
				
				}
			/>
		)
	}

}