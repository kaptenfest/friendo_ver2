import React, { Component, PropTypes } from 'react';
import axios from "axios";
import AppCreate from "../create_event";
import {Link} from "react-router";

export default class Sidebar extends Component {

	handleClick(e) {
		e.preventDefault()
		
		this.context.router.push('create-event');
		setTimeout(() => { document.getElementById("modal").className = "modal";
		 }, 1);
		
	}
	
	logout() {
		localStorage.removeItem("user_id")
		localStorage.removeItem("token")
		location.reload();
	}
	
	render() {
		
	return (
		

		<div className="left-sidebar" id="left-sidebar">
			<div className="logo" id="logo">
				<img src="../img/sidebar/logo2.png" />
			</div>
			<div className="create-event" id="btn-create">
				<button onClick={this.handleClick.bind(this)}><span> <strong>+</strong> CREATE EVENTS</span></button>
			</div>
			<div className="sitenav" id="sitenav">
				<nav>
					<ul>
						<li><a id="navitem1" href="#"><img src="/img/sidebar/events_active.png" /><span>EVENTS</span></a></li>
						<li><a id="navitem2" href="#"><img src="/img/sidebar/Profile.png" /><span>PROFILE</span></a></li>
						<li><a id="navitem3" href="#"><img src="/img/sidebar/Settings.png" /><span>SETTINGS</span></a></li>
					</ul>
				</nav>
			</div>
			<div className="bottom-navigation navbar-fixed-bottom" id="bottom-navigation">
				<nav>
					<ul>
						<li><a id="bottomnav1" href="#"><img src="/img/sidebar/alerts.png" /><span>ALERTS</span> - 42</a></li>
						<li><a id="bottomnav2" href="#"><img src="/img/sidebar/feedback.png" /><span>SEND FEEDBACK</span></a></li>
						<li id="bottomnav3" onClick={this.logout.bind(this)}><img src="/img/sidebar/logout.png" /><span>LOGOUT</span></li>
					</ul>
				</nav>
			</div>
		</div>
		)
	}
}

Sidebar.contextTypes = {
  router: PropTypes.object.isRequired
};

