	// StyleSheets
require("../css/00-global.css");
require("../css/01-login.css");
require("../css/02-create-event.css");
require("../css/03-sidebar.css");
require("../css/04-eventOverview.css");
require("../css/05-eventDetails.css");
require("../css/06-calendar.css");
require("../css/ext_mod/datetime.css");
require("../css/ext_mod/geosuggest.css");


// React
import React from 'react';
import ReactDOM from 'react-dom';

// React Bootstrap
import { Grid, Row, Col, Clearfix } from 'react-bootstrap';

// Redux
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxPromise from "redux-promise";
import reducers from './reducers';

import createLogger from 'redux-logger';

// React Router
import {Router, Route, IndexRoute, Link, IndexLink, IndexRedirect, hashHistory} from 'react-router';

// Components / Containers
import App from './components/app';
import AppCreate from '../js/create_event.js';
import Details from '../js/modules_create_event/details.js';
import Eventdate from '../js/modules_create_event/eventdate.js';
import Eventlocation from '../js/modules_create_event/eventlocation.js';
import People from '../js/modules_create_event/people.js';
import EventOverview from './containers/eventOverview';
import EventDetails from './containers/eventDetails';
import Login from '../login/login';
import Register from '../login/register';

const logger = createLogger();
const createStoreWithMiddleware = applyMiddleware(ReduxPromise, logger)(createStore);
const app = document.getElementById("app");

var loggedIn = false;
if(localStorage.user_id) {
	loggedIn = true;
}

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
	  <Router history={hashHistory}>
	  	  <Route path='register' component={Register}/>
		  <Route path='/' component={loggedIn ? App : Login}>
		     <Route path='create-event' component={AppCreate}>
			 	  <IndexRedirect to="Details"/>
			 	  <Route path="details" component={Details}/>
				  <Route path="eventdate" component={Eventdate}/>
				  <Route path="eventlocation" component={Eventlocation}/>
				  <Route path="people" component={People}/>
				</Route>
			</Route>
			  <Route path="eventDetails/:theEventID" component={EventDetails}/>    
	   </Router>
	 </Provider>,
 app

);