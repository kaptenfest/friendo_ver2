// React
import React, { Component } from "react";

// Redux
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

// Actions
import { fetchEvent } from "../actions/index.js";
import { sortEvents } from "../actions/index.js";

// Containers/Components
import { EventChoice } from "./eventChoice";
import { EventDetails } from "./eventDetails";
import Sidebar from "../components/Sidebar";

// React Router
import {Router, Route, Link, hashHistory } from 'react-router';

// --------------------------------------------------------------------

// FRÅN INLOGG - - - - - - - - - - - - -

// Hämta user_ID från Localstorage
const user_id = localStorage.getItem("user_id")
// console.log(user_id)
	
// Hämta token från Localstorage
const token = localStorage.getItem("token")
// console.log(token)

// - - - - - - - - - - - - - - - - - 

// Arrayer som samlar events för filter
var upcomingArray = [];
var goingArray = [];
var notGoingArray = [];
var notAnsweredArray = [];
var hostedArray = [];

// För att få unika keys varje gång
var keyGenerator = 42

class EventOverview extends Component {
		constructor(props){
		super(props)
		this.updateOverview = this.updateOverview.bind(this)

		this.state = {
			fetchedEvents: "",
			filteredEvents: ""
		}
	}
	
	// Hämtar allla events
	// Kallar på fetchEvent action med @param - user_id som ligger i localstorage
	componentWillMount() {
		this.props.fetchEvent(user_id); 
	}

	// Sätter alla events i fetchedEvents som används för att filtrera events
	componentDidUpdate(){
		if(this.state.fetchedEvents === ""){
			this.setState({fetchedEvents: this.props.event})
		}
		else {
			console.log("State is set...")
		}
	}
	
	updateOverview(){
		location.reload()
	}
	
	// Rendrerar alla events
	// @param dataEvent - är en mappning av alla events
	// @import EventChoice - dropdown som skickar en action om man kommer eller inte
	renderCard(dataEvent){
		return (
			<article key={keyGenerator++} id={dataEvent.status} className="event-container">
				<div className="event-image-container">
				<Link to={`/eventDetails/${dataEvent.id}`}><img className="event-image" src={"http://app.friendoapp.com/" + dataEvent.image}></img></Link>
				</div>
			<div className="event-info">
				<Link to={`/eventDetails/${dataEvent.id}`}><h2>{dataEvent.name}</h2></Link>
			</div>
			<div className="first">
				<div className="event-date"><img src="../img/events/clock.png" />{self.fixTime(dataEvent.start_time)}</div>
				<div className="participants"><img src="../img/events/people.png" /><p>{dataEvent.count_join}</p></div>
			</div>
			<div className="second">
				{self.shortLocation(dataEvent.location)}
				 
				 <EventChoice eventID={dataEvent.id} eventStatus={dataEvent.status} updateOverviewProp={self.updateOverview} token={token}/>

			</div>
			</article>
		)
	}

	addZero(i) {
  	if (i < 10) {
    	i = "0" + i;
  	}
  	return i;
	}

	// Konverterar UNIX tid till läslig tid
	// @param startTime - mappning av starttiden i events
	// return time - ex. 01 Januari, 12.00
	fixTime(startTime){	
		if(startTime === ""){
			return <p>Voting in progress...</p>
		}

		var a = new Date(Number(startTime));
 		var months = ['January','February','Mars','April','May','June','July','August','September','October','November','December'];
 		var month = months[a.getMonth()];
 		var date = self.addZero(a.getDate());
 		var hour = self.addZero(a.getHours());
 		var min = self.addZero(a.getMinutes());
 		var time = date + ' ' + month + ', ' + hour + ':' + min;
 			return time;
	}

	// Rendrerar "titeln" på platsen
	// @param location - mappning av location arrayen i events
	// return shorty - ex. Slussen
	shortLocation(location){
		if(location === " Omröstning pågår"){
			return <div className="event-location"><img src="../img/events/pin.png" /><p>Voting in progress</p></div>
		}
		if(location === " Väntar på omröstning ..."){
			return <div className="event-location"><img src="../img/events/pin.png" /><p>Start voting...</p></div>
		}
		if(location == " Ej ifyllt"){
			return <div className="event-location"><img src="../img/events/pin.png" /><p>No location suggested</p></div>
		}
		if(!location.includes("§")){
			return <div className="event-location"><img src="../img/events/pin.png" /><p>{location}</p></div>
		}

		var shorty = location;
		shorty = shorty.substring(0, shorty.indexOf('§'));
			return (
				<div className="event-location"><img src="../img/events/pin.png" />{shorty}</div>
			)
		}


	// Filtrerar ut valt alternativ och skickar till arrayer
	selectedValue(){
		var theFilter = document.getElementById('filterEvents').value;

		if(theFilter === "upcoming"){
			var comingEvents = this.state.fetchedEvents.map(this.upcoming);
				this.setState({filteredEvents: upcomingArray})
				upcomingArray = []
		}

		if(theFilter === "attendingMaybe"){
			var gogogo = this.state.fetchedEvents.filter(this.going);
				this.setState({filteredEvents: goingArray})
				goingArray = []
		}

		if(theFilter === "notAttending"){
			var noWay = this.state.fetchedEvents.filter(this.notGoing)
			this.setState({filteredEvents: notGoingArray})
			notGoingArray = []
		}

		if(theFilter === "unanswered"){
			var noAnswerHere = this.state.fetchedEvents.filter(this.noAnswer)
			this.setState({filteredEvents: notAnsweredArray})
			notAnsweredArray = []
		}

		if(theFilter === "hosted"){
			var host = this.state.fetchedEvents.filter(this.hostedEvents)
			this.setState({filteredEvents: hostedArray})
			hostedArray = []
		}
	}

	// @param future - mappar över fetchedEvents arrayen
	upcoming(future){
		upcomingArray.push(future)
	}

	// @param go - filtrerar fetchEvents arrayen
	going(go){
		if(go.status === "join" || go.status === "can_join"){	
			goingArray.push(go);
		}
	}

	// @param noGo - filtrerar fetchEvents arrayen
	notGoing(noGo){
		if(noGo.status === "un_join"){
			notGoingArray.push(noGo)
		}
	}

	// @param notYet - filtrerar fetchEvents arrayen
	noAnswer(notYet){
		if(notYet.status === "no_join"){
		notAnsweredArray.push(notYet)
		}
	}

	// @param theHost - filtrerar fetchEvents arrayen
	hostedEvents(theHost){
		if(theHost.hostId === user_id){
		hostedArray.push(theHost)
		}
	}

	// Döljer sidebar på mobilversionen
	toggleSidebar() {
		let sidebar = document.getElementById('left-sidebar');
		sidebar.className === "left-sidebar-visible" ? sidebar.className = "left-sidebar" : sidebar.className = "left-sidebar-visible";
	}

	render(){

		self=this;

		return(
			<div id="event-overview">
				<div className="event-header">
					<div id="sidebar-toggle"
						 onClick={this.toggleSidebar.bind(this)}>
						<img src="../../img/sidebar/menu-toggle.png" alt="menutoggler"/>
					</div>
					<div className="event-subHeader"><img src="../img/events/events.png" /><h1><span>EVENTS</span></h1></div>
						<select id="filterEvents" onChange={this.selectedValue.bind(this)}>
							<option value="upcoming">Upcoming events</option>
							<option value="attendingMaybe">Attending/Maybe</option>
							<option value="notAttending">Not attending</option>
							<option value="unanswered">Unanswered</option>
							<option value="hosted">Hosted events</option>
						</select>
				</div>
				<div className="centerDiv">
						<div className="events">
							{this.state.filteredEvents === "" ? this.props.event.map(this.renderCard) : this.state.filteredEvents.map(this.renderCard)}

						</div>
				</div>
			</div>
			);
	}
}

function mapStateToProps(state){
	return { event: state.event };
}

function mapDispatchToProps(dispatch){
	return bindActionCreators ({ fetchEvent, sortEvents }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps) (EventOverview);