import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

// Importerar action för att posta "Röstade platser" till API
import { choosePlace } from "../actions/index";

// -------------------------------------------------------------------------

export class PlacesChoice extends Component {

	handleChange(){
		choosePlace(this.props.placeID, this.props.token);
		this.props.refresh();
	}


		// Kontrollerar om användaren har röstat
	checkChecked(){
		if(this.props.myPlaceVote === "1"){
			return true;
		}

		else {
			return false;
		}
	}


	render(){

		self=this

		var checked = self.checkChecked();

		return(
		
			<div className="placesVoteHolder">
				
				<div className="placesVoter">
					<label htmlFor="voteForPlace"></label>
						<label className="placesCheckbox">
    				<input type="checkbox" name="voteForPlace" checked={checked} value={this.props.placeID} onChange={this.handleChange.bind(this)} />
   					<span></span>
					</label>
				</div>
				
				<div className="placesVotes">
					<p>{this.props.placeCount}</p>
				</div>

			</div>

		)
	
	}
}

	PlacesChoice.propTypes = {
  refresh: React.PropTypes.func,
	};