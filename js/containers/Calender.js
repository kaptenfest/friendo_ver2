// React
import React, { Component } from "react";

// Redux
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

// Actions
import { fetchEventDates } from "../actions/index.js";

// Moment.js ( Date )
import moment from 'moment';



// FRÅN INLOGG - - - - - - - - - - - - -

// Hämta user_ID från Localstorage
const user_id = localStorage.getItem("user_id")
// console.log(user_id)
	
// Hämta token från Localstorage
const token = localStorage.getItem("token")
// console.log(token)

// - - - - - - - - - - - - - - - - - 


class Calender extends Component {

	componentWillMount(){
			this.props.fetchEventDates(token)
	}
	
	// Första Månaden 
	oneWeekOne(oneFirstWeek){
		var arrOne = [];

		for(var i = 0; i < oneFirstWeek.length; i++){
			
			// Dagens datum
			if(moment().format("YYYY-MM-DD") === oneFirstWeek[i]){
				arrOne.push(<td className="today" key={Math.random()}>{oneFirstWeek[i].substr(8,2)}</td>)
			}

			// Dag utan event
			else {
				arrOne.push(<td className="regularDay" key={Math.random()}>{oneFirstWeek[i].substr(8,2)}</td>)
			}

			for(var j = 0; j < self.props.eventDate.length; j++){
				
				// Dag med event
				if(oneFirstWeek[i] === self.props.eventDate[j]){
					arrOne.pop()
					arrOne.push(<td className="eventDay" key={Math.random()}>{oneFirstWeek[i].substr(8,2)}</td>)
				}

				// Dagens datum och ett event
				if(oneFirstWeek[i] === self.props.eventDate[j] && self.props.eventDate[j] === moment().format("YYYY-MM-DD")){
					arrOne.pop()
					arrOne.push(<td className="eventDayToday" key={Math.random()}>{oneFirstWeek[i].substr(8,2)}</td>)
				}

			}
		}

		return arrOne
	}

	
	oneWeekTwo(oneSecondWeek){
		var arrTwo = [];
		
		for(var i = 0; i < oneSecondWeek.length; i++){
			
			// Dagens datum
			if(moment().format("YYYY-MM-DD") === oneSecondWeek[i]){
				arrTwo.push(<td className="today" key={Math.random()}>{oneSecondWeek[i].substr(8,2)}</td>)
			}

			// Dag utan event
			else {
				arrTwo.push(<td className="regularDay" key={Math.random()}>{oneSecondWeek[i].substr(8,2)}</td>)
			}

			for(var j = 0; j < self.props.eventDate.length; j++){
				
				// Dag med event
				if(oneSecondWeek[i] === self.props.eventDate[j]){
					arrTwo.pop()
					arrTwo.push(<td className="eventDay" key={Math.random()}>{oneSecondWeek[i].substr(8,2)}</td>)
				}

				// Dagens datum och ett event
				if(oneSecondWeek[i] === self.props.eventDate[j] && self.props.eventDate[j] === moment().format("YYYY-MM-DD")){
					arrTwo.pop()
					arrTwo.push(<td className="eventDayToday" key={Math.random()}>{oneSecondWeek[i].substr(8,2)}</td>)
				}

			}
		}

		return arrTwo
	}

	oneWeekThree(oneThirdWeek){
		var arrThree = [];
		
		for(var i = 0; i < oneThirdWeek.length; i++){
			
			// Dagens datum
			if(moment().format("YYYY-MM-DD") === oneThirdWeek[i]){
				arrThree.push(<td className="today" key={Math.random()}>{oneThirdWeek[i].substr(8,2)}</td>)
			}

			// Dag utan event
			else {
				arrThree.push(<td className="regularDay" key={Math.random()}>{oneThirdWeek[i].substr(8,2)}</td>)
			}

			for(var j = 0; j < self.props.eventDate.length; j++){
				
				// Dag med event
				if(oneThirdWeek[i] === self.props.eventDate[j]){
					arrThree.pop()
					arrThree.push(<td className="eventDay" key={Math.random()}>{oneThirdWeek[i].substr(8,2)}</td>)
				}

				// Dagens datum och ett event
				if(oneThirdWeek[i] === self.props.eventDate[j] && self.props.eventDate[j] === moment().format("YYYY-MM-DD")){
					arrThree.pop()
					arrThree.push(<td className="eventDayToday" key={Math.random()}>{oneThirdWeek[i].substr(8,2)}</td>)
				}

			}
		}

		return arrThree
	}

	
	oneWeekFour(oneFourthWeek){

		var arrFour = [];

		for(var i = 0; i < oneFourthWeek.length; i++){
			
			// Dagens datum
			if(moment().format("YYYY-MM-DD") === oneFourthWeek[i]){
				arrFour.push(<td className="today" key={Math.random()}>{oneFourthWeek[i].substr(8,2)}</td>)
			}

			// Dag utan event
			else {
				arrFour.push(<td className="regularDay" key={Math.random()}>{oneFourthWeek[i].substr(8,2)}</td>)
			}

			for(var j = 0; j < self.props.eventDate.length; j++){
				
				// Dag med event
				if(oneFourthWeek[i] === self.props.eventDate[j]){
					arrFour.pop()
					arrFour.push(<td className="eventDay" key={Math.random()}>{oneFourthWeek[i].substr(8,2)}</td>)
				}

				// Dagens datum och ett event
				if(oneFourthWeek[i] === self.props.eventDate[j] && self.props.eventDate[j] === moment().format("YYYY-MM-DD")){
					arrFour.pop()
					arrFour.push(<td className="eventDayToday" key={Math.random()}>{oneFourthWeek[i].substr(8,2)}</td>)
				}

			}
		}

		return arrFour

	}

	oneWeekFive(oneFifthWeek){
		var arrFive = [];
		
		for(var i = 0; i < oneFifthWeek.length; i++){
			
			// Dagens datum
			if(moment().format("YYYY-MM-DD") === oneFifthWeek[i]){
				arrFive.push(<td className="today" key={Math.random()}>{oneFifthWeek[i].substr(8,2)}</td>)
			}

			// Dag utan event
			else {
				arrFive.push(<td className="regularDay" key={Math.random()}>{oneFifthWeek[i].substr(8,2)}</td>)
			}

			for(var j = 0; j < self.props.eventDate.length; j++){
				
				// Dag med event
				if(oneFifthWeek[i] === self.props.eventDate[j]){
					arrFive.pop()
					arrFive.push(<td className="eventDay" key={Math.random()}>{oneFifthWeek[i].substr(8,2)}</td>)
				}

				// Dagens datum och ett event
				if(oneFifthWeek[i] === self.props.eventDate[j] && self.props.eventDate[j] === moment().format("YYYY-MM-DD")){
					arrFive.pop()
					arrFive.push(<td className="eventDayToday" key={Math.random()}>{oneFifthWeek[i].substr(8,2)}</td>)
				}

			}
		}

		return arrFive
	}

		oneWeekSix(oneSixthWeek){
		var arrFiveExtra = [];
		
		for(var i = 0; i < oneSixthWeek.length; i++){
			
			// Dagens datum
			if(moment().format("YYYY-MM-DD") === oneSixthWeek[i]){
				arrFiveExtra.push(<td className="today" key={Math.random()}>{oneSixthWeek[i].substr(8,2)}</td>)
			}

			// Dag utan event
			else {
				arrFiveExtra.push(<td className="regularDay" key={Math.random()}>{oneSixthWeek[i].substr(8,2)}</td>)
			}

			for(var j = 0; j < self.props.eventDate.length; j++){
				
				// Dag med event
				if(oneSixthWeek[i] === self.props.eventDate[j]){
					arrFiveExtra.pop()
					arrFiveExtra.push(<td className="eventDay" key={Math.random()}>{oneSixthWeek[i].substr(8,2)}</td>)
				}

				// Dagens datum och ett event
				if(oneSixthWeek[i] === self.props.eventDate[j] && self.props.eventDate[j] === moment().format("YYYY-MM-DD")){
					arrFiveExtra.pop()
					arrFiveExtra.push(<td className="eventDayToday" key={Math.random()}>{oneSixthWeek[i].substr(8,2)}</td>)
				}

			}
		}

		return arrFiveExtra
	}


	// Andra Månaden
	twoWeekOne(twoWeekOneDays){
		var arrSix = [];
		
		for(var i = 0; i < twoWeekOneDays.length; i++){
			
			// Dagens datum
			if(moment().format("YYYY-MM-DD") === twoWeekOneDays[i]){
				arrSix.push(<td className="today" key={Math.random()}>{twoWeekOneDays[i].substr(8,2)}</td>)
			}

			// Dag utan event
			else {
				arrSix.push(<td className="regularDay" key={Math.random()}>{twoWeekOneDays[i].substr(8,2)}</td>)
			}

			for(var j = 0; j < self.props.eventDate.length; j++){
				
				// Dag med event
				if(twoWeekOneDays[i] === self.props.eventDate[j]){
					arrSix.pop()
					arrSix.push(<td className="eventDay" key={Math.random()}>{twoWeekOneDays[i].substr(8,2)}</td>)
				}

				// Dagens datum och ett event
				if(twoWeekOneDays[i] === self.props.eventDate[j] && self.props.eventDate[j] === moment().format("YYYY-MM-DD")){
					arrSix.pop()
					arrSix.push(<td className="eventDayToday" key={Math.random()}>{twoWeekOneDays[i].substr(8,2)}</td>)
				}

			}
		}

		return arrSix
	}

	twoWeekTwo(twoWeekTwoDays){
		var arrSeven = [];

		for(var i = 0; i < twoWeekTwoDays.length; i++){
			
			// Dagens datum
			if(moment().format("YYYY-MM-DD") === twoWeekTwoDays[i]){
				arrSeven.push(<td className="today" key={Math.random()}>{twoWeekTwoDays[i].substr(8,2)}</td>)
			}

			// Dag utan event
			else {
				arrSeven.push(<td className="regularDay" key={Math.random()}>{twoWeekTwoDays[i].substr(8,2)}</td>)
			}

			for(var j = 0; j < self.props.eventDate.length; j++){
				
				// Dag med event
				if(twoWeekTwoDays[i] === self.props.eventDate[j]){
					arrSeven.pop()
					arrSeven.push(<td className="eventDay" key={Math.random()}>{twoWeekTwoDays[i].substr(8,2)}</td>)
				}

				// Dagens datum och ett event
				if(twoWeekTwoDays[i] === self.props.eventDate[j] && self.props.eventDate[j] === moment().format("YYYY-MM-DD")){
					arrSeven.pop()
					arrSeven.push(<td className="eventDayToday" key={Math.random()}>{twoWeekTwoDays[i].substr(8,2)}</td>)
				}

			}
		}

		return arrSeven
	}

	twoWeekThree(twoWeekThreeDays){
		var arrEight = [];

		for(var i = 0; i < twoWeekThreeDays.length; i++){
			
			// Dagens datum
			if(moment().format("YYYY-MM-DD") === twoWeekThreeDays[i]){
				arrEight.push(<td className="today" key={Math.random()}>{twoWeekThreeDays[i].substr(8,2)}</td>)
			}

			// Dag utan event
			else {
				arrEight.push(<td className="regularDay" key={Math.random()}>{twoWeekThreeDays[i].substr(8,2)}</td>)
			}

			for(var j = 0; j < self.props.eventDate.length; j++){
				
				// Dag med event
				if(twoWeekThreeDays[i] === self.props.eventDate[j]){
					arrEight.pop()
					arrEight.push(<td className="eventDay" key={Math.random()}>{twoWeekThreeDays[i].substr(8,2)}</td>)
				}

				// Dagens datum och ett event
				if(twoWeekThreeDays[i] === self.props.eventDate[j] && self.props.eventDate[j] === moment().format("YYYY-MM-DD")){
					arrEight.pop()
					arrEight.push(<td className="eventDayToday" key={Math.random()}>{twoWeekThreeDays[i].substr(8,2)}</td>)
				}

			}
		}

		return arrEight
	}

	twoWeekFour(twoWeekFourDays){
		var arrNine = [];

		for(var i = 0; i < twoWeekFourDays.length; i++){
			
			// Dagens datum
			if(moment().format("YYYY-MM-DD") === twoWeekFourDays[i]){
				arrNine.push(<td className="today" key={Math.random()}>{twoWeekFourDays[i].substr(8,2)}</td>)
			}

			// Dag utan event
			else {
				arrNine.push(<td className="regularDay" key={Math.random()}>{twoWeekFourDays[i].substr(8,2)}</td>)
			}

			for(var j = 0; j < self.props.eventDate.length; j++){
				
				// Dag med event
				if(twoWeekFourDays[i] === self.props.eventDate[j]){
					arrNine.pop()
					arrNine.push(<td className="eventDay" key={Math.random()}>{twoWeekFourDays[i].substr(8,2)}</td>)
				}

				// Dagens datum och ett event
				if(twoWeekFourDays[i] === self.props.eventDate[j] && self.props.eventDate[j] === moment().format("YYYY-MM-DD")){
					arrNine.pop()
					arrNine.push(<td className="eventDayToday" key={Math.random()}>{twoWeekFourDays[i].substr(8,2)}</td>)
				}

			}
		}

		return arrNine
	}

		twoWeekFive(twoWeekFiveDays){
		var arrTen = [];

		for(var i = 0; i < twoWeekFiveDays.length; i++){
			
			// Dagens datum
			if(moment().format("YYYY-MM-DD") === twoWeekFiveDays[i]){
				arrTen.push(<td className="today" key={Math.random()}>{twoWeekFiveDays[i].substr(8,2)}</td>)
			}

			// Dag utan event
			else {
				arrTen.push(<td className="regularDay" key={Math.random()}>{twoWeekFiveDays[i].substr(8,2)}</td>)
			}

			for(var j = 0; j < self.props.eventDate.length; j++){
				
				// Dag med event
				if(twoWeekFiveDays[i] === self.props.eventDate[j]){
					arrTen.pop()
					arrTen.push(<td className="eventDay" key={Math.random()}>{twoWeekFiveDays[i].substr(8,2)}</td>)
				}

				// Dagens datum och ett event
				if(twoWeekFiveDays[i] === self.props.eventDate[j] && self.props.eventDate[j] === moment().format("YYYY-MM-DD")){
					arrTen.pop()
					arrTen.push(<td className="eventDayToday" key={Math.random()}>{twoWeekFiveDays[i].substr(8,2)}</td>)
				}

			}
		}

		return arrTen
	}

			twoWeekSix(twoWeekSixDays){
		var arrTenExtra = [];

		for(var i = 0; i < twoWeekSixDays.length; i++){
			
			// Dagens datum
			if(moment().format("YYYY-MM-DD") === twoWeekSixDays[i]){
				arrTenExtra.push(<td className="today" key={Math.random()}>{twoWeekSixDays[i].substr(8,2)}</td>)
			}

			// Dag utan event
			else {
				arrTenExtra.push(<td className="regularDay" key={Math.random()}>{twoWeekSixDays[i].substr(8,2)}</td>)
			}

			for(var j = 0; j < self.props.eventDate.length; j++){
				
				// Dag med event
				if(twoWeekSixDays[i] === self.props.eventDate[j]){
					arrTenExtra.pop()
					arrTenExtra.push(<td className="eventDay" key={Math.random()}>{twoWeekSixDays[i].substr(8,2)}</td>)
				}

				// Dagens datum och ett event
				if(twoWeekSixDays[i] === self.props.eventDate[j] && self.props.eventDate[j] === moment().format("YYYY-MM-DD")){
					arrTenExtra.pop()
					arrTenExtra.push(<td className="eventDayToday" key={Math.random()}>{twoWeekSixDays[i].substr(8,2)}</td>)
				}

			}
		}

		return arrTenExtra
	}


	// Tredje Månaden
	threeWeekOne(threeWeekOneDays){
		var arrEleven = [];

		for(var i = 0; i < threeWeekOneDays.length; i++){
			
			// Dagens datum
			if(moment().format("YYYY-MM-DD") === threeWeekOneDays[i]){
				arrEleven.push(<td className="today" key={Math.random()}>{threeWeekOneDays[i].substr(8,2)}</td>)
			}

			// Dag utan event
			else {
				arrEleven.push(<td className="regularDay" key={Math.random()}>{threeWeekOneDays[i].substr(8,2)}</td>)
			}

			for(var j = 0; j < self.props.eventDate.length; j++){
				
				// Dag med event
				if(threeWeekOneDays[i] === self.props.eventDate[j]){
					arrEleven.pop()
					arrEleven.push(<td className="eventDay" key={Math.random()}>{threeWeekOneDays[i].substr(8,2)}</td>)
				}

				// Dagens datum och ett event
				if(threeWeekOneDays[i] === self.props.eventDate[j] && self.props.eventDate[j] === moment().format("YYYY-MM-DD")){
					arrEleven.pop()
					arrEleven.push(<td className="eventDayToday" key={Math.random()}>{threeWeekOneDays[i].substr(8,2)}</td>)
				}

			}
		}

		return arrEleven
	}

	threeWeekTwo(threeWeekTwoDays){
		var arrTwelve = [];

		for(var i = 0; i < threeWeekTwoDays.length; i++){
			
			// Dagens datum
			if(moment().format("YYYY-MM-DD") === threeWeekTwoDays[i]){
				arrTwelve.push(<td className="today" key={Math.random()}>{threeWeekTwoDays[i].substr(8,2)}</td>)
			}

			// Dag utan event
			else {
				arrTwelve.push(<td className="regularDay" key={Math.random()}>{threeWeekTwoDays[i].substr(8,2)}</td>)
			}

			for(var j = 0; j < self.props.eventDate.length; j++){
				
				// Dag med event
				if(threeWeekTwoDays[i] === self.props.eventDate[j]){
					arrTwelve.pop()
					arrTwelve.push(<td className="eventDay" key={Math.random()}>{threeWeekTwoDays[i].substr(8,2)}</td>)
				}

				// Dagens datum och ett event
				if(threeWeekTwoDays[i] === self.props.eventDate[j] && self.props.eventDate[j] === moment().format("YYYY-MM-DD")){
					arrTwelve.pop()
					arrTwelve.push(<td className="eventDayToday" key={Math.random()}>{threeWeekTwoDays[i].substr(8,2)}</td>)
				}

			}
		}

		return arrTwelve
	}

	threeWeekThree(threeWeekThreeDays){
		var arrThirteen = [];

		for(var i = 0; i < threeWeekThreeDays.length; i++){
			
			// Dagens datum
			if(moment().format("YYYY-MM-DD") === threeWeekThreeDays[i]){
				arrThirteen.push(<td className="today" key={Math.random()}>{threeWeekThreeDays[i].substr(8,2)}</td>)
			}

			// Dag utan event
			else {
				arrThirteen.push(<td className="regularDay" key={Math.random()}>{threeWeekThreeDays[i].substr(8,2)}</td>)
			}

			for(var j = 0; j < self.props.eventDate.length; j++){
				
				// Dag med event
				if(threeWeekThreeDays[i] === self.props.eventDate[j]){
					arrThirteen.pop()
					arrThirteen.push(<td className="eventDay" key={Math.random()}>{threeWeekThreeDays[i].substr(8,2)}</td>)
				}

				// Dagens datum och ett event
				if(threeWeekThreeDays[i] === self.props.eventDate[j] && self.props.eventDate[j] === moment().format("YYYY-MM-DD")){
					arrThirteen.pop()
					arrThirteen.push(<td className="eventDayToday" key={Math.random()}>{threeWeekThreeDays[i].substr(8,2)}</td>)
				}

			}
		}

		return arrThirteen
	}

	threeWeekFour(threeWeekFourDays){
		var arrFourteen = [];

		for(var i = 0; i < threeWeekFourDays.length; i++){
			
			// Dagens datum
			if(moment().format("YYYY-MM-DD") === threeWeekFourDays[i]){
				arrFourteen.push(<td className="today" key={Math.random()}>{threeWeekFourDays[i].substr(8,2)}</td>)
			}

			// Dag utan event
			else {
				arrFourteen.push(<td className="regularDay" key={Math.random()}>{threeWeekFourDays[i].substr(8,2)}</td>)
			}

			for(var j = 0; j < self.props.eventDate.length; j++){
				
				// Dag med event
				if(threeWeekFourDays[i] === self.props.eventDate[j]){
					arrFourteen.pop()
					arrFourteen.push(<td className="eventDay" key={Math.random()}>{threeWeekFourDays[i].substr(8,2)}</td>)
				}

				// Dagens datum och ett event
				if(threeWeekFourDays[i] === self.props.eventDate[j] && self.props.eventDate[j] === moment().format("YYYY-MM-DD")){
					arrFourteen.pop()
					arrFourteen.push(<td className="eventDayToday" key={Math.random()}>{threeWeekFourDays[i].substr(8,2)}</td>)
				}

			}
		}

		return arrFourteen
	}

	threeWeekFive(threeWeekFiveDays){
		var arrFifteen = [];

		for(var i = 0; i < threeWeekFiveDays.length; i++){
			
			// Dagens datum
			if(moment().format("YYYY-MM-DD") === threeWeekFiveDays[i]){
				arrFifteen.push(<td className="today" key={Math.random()}>{threeWeekFiveDays[i].substr(8,2)}</td>)
			}

			// Dag utan event
			else {
				arrFifteen.push(<td className="regularDay" key={Math.random()}>{threeWeekFiveDays[i].substr(8,2)}</td>)
			}

			for(var j = 0; j < self.props.eventDate.length; j++){
				
				// Dag med event
				if(threeWeekFiveDays[i] === self.props.eventDate[j]){
					arrFifteen.pop()
					arrFifteen.push(<td className="eventDay" key={Math.random()}>{threeWeekFiveDays[i].substr(8,2)}</td>)
				}

				// Dagens datum och ett event
				if(threeWeekFiveDays[i] === self.props.eventDate[j] && self.props.eventDate[j] === moment().format("YYYY-MM-DD")){
					arrFifteen.pop()
					arrFifteen.push(<td className="eventDayToday" key={Math.random()}>{threeWeekFiveDays[i].substr(8,2)}</td>)
				}

			}
		}

		return arrFifteen
	}

		threeWeekSix(threeWeekSixthDays){
		var arrFifteenExtra = [];

		for(var i = 0; i < threeWeekSixthDays.length; i++){
			
			// Dagens datum
			if(moment().format("YYYY-MM-DD") === threeWeekSixthDays[i]){
				arrFifteenExtra.push(<td className="today" key={Math.random()}>{threeWeekSixthDays[i].substr(8,2)}</td>)
			}

			// Dag utan event
			else {
				arrFifteenExtra.push(<td className="regularDay" key={Math.random()}>{threeWeekSixthDays[i].substr(8,2)}</td>)
			}

			for(var j = 0; j < self.props.eventDate.length; j++){
				
				// Dag med event
				if(threeWeekSixthDays[i] === self.props.eventDate[j]){
					arrFifteenExtra.pop()
					arrFifteenExtra.push(<td className="eventDay" key={Math.random()}>{threeWeekSixthDays[i].substr(8,2)}</td>)
				}

				// Dagens datum och ett event
				if(threeWeekSixthDays[i] === self.props.eventDate[j] && self.props.eventDate[j] === moment().format("YYYY-MM-DD")){
					arrFifteenExtra.pop()
					arrFifteenExtra.push(<td className="eventDayToday" key={Math.random()}>{threeWeekSixthDays[i].substr(8,2)}</td>)
				}

			}
		}

		return arrFifteenExtra
	}

	// Fjärde Månaden
	fourWeekOne(fourWeekOneDays){
		var arrSixteen = [];

		for(var i = 0; i < fourWeekOneDays.length; i++){
			
			// Dagens datum
			if(moment().format("YYYY-MM-DD") === fourWeekOneDays[i]){
				arrSixteen.push(<td className="today" key={Math.random()}>{fourWeekOneDays[i].substr(8,2)}</td>)
			}

			// Dag utan event
			else {
				arrSixteen.push(<td className="regularDay" key={Math.random()}>{fourWeekOneDays[i].substr(8,2)}</td>)
			}

			for(var j = 0; j < self.props.eventDate.length; j++){
				
				// Dag med event
				if(fourWeekOneDays[i] === self.props.eventDate[j]){
					arrSixteen.pop()
					arrSixteen.push(<td className="eventDay" key={Math.random()}>{fourWeekOneDays[i].substr(8,2)}</td>)
				}

				// Dagens datum och ett event
				if(fourWeekOneDays[i] === self.props.eventDate[j] && self.props.eventDate[j] === moment().format("YYYY-MM-DD")){
					arrSixteen.pop()
					arrSixteen.push(<td className="eventDayToday" key={Math.random()}>{fourWeekOneDays[i].substr(8,2)}</td>)
				}

			}
		}

		return arrSixteen
	}

	fourWeekTwo(fourWeekTwoDays){
		var arrSeventeen = [];

		for(var i = 0; i < fourWeekTwoDays.length; i++){
			
			// Dagens datum
			if(moment().format("YYYY-MM-DD") === fourWeekTwoDays[i]){
				arrSeventeen.push(<td className="today" key={Math.random()}>{fourWeekTwoDays[i].substr(8,2)}</td>)
			}

			// Dag utan event
			else {
				arrSeventeen.push(<td className="regularDay" key={Math.random()}>{fourWeekTwoDays[i].substr(8,2)}</td>)
			}

			for(var j = 0; j < self.props.eventDate.length; j++){
				
				// Dag med event
				if(fourWeekTwoDays[i] === self.props.eventDate[j]){
					arrSeventeen.pop()
					arrSeventeen.push(<td className="eventDay" key={Math.random()}>{fourWeekTwoDays[i].substr(8,2)}</td>)
				}

				// Dagens datum och ett event
				if(fourWeekTwoDays[i] === self.props.eventDate[j] && self.props.eventDate[j] === moment().format("YYYY-MM-DD")){
					arrSeventeen.pop()
					arrSeventeen.push(<td className="eventDayToday" key={Math.random()}>{fourWeekTwoDays[i].substr(8,2)}</td>)
				}

			}
		}

		return arrSeventeen
	}

	fourWeekThree(fourWeekThreeDays){
		var arrEighteen = [];

		for(var i = 0; i < fourWeekThreeDays.length; i++){
			
			// Dagens datum
			if(moment().format("YYYY-MM-DD") === fourWeekThreeDays[i]){
				arrEighteen.push(<td className="today" key={Math.random()}>{fourWeekThreeDays[i].substr(8,2)}</td>)
			}

			// Dag utan event
			else {
				arrEighteen.push(<td className="regularDay" key={Math.random()}>{fourWeekThreeDays[i].substr(8,2)}</td>)
			}

			for(var j = 0; j < self.props.eventDate.length; j++){
				
				// Dag med event
				if(fourWeekThreeDays[i] === self.props.eventDate[j]){
					arrEighteen.pop()
					arrEighteen.push(<td className="eventDay" key={Math.random()}>{fourWeekThreeDays[i].substr(8,2)}</td>)
				}

				// Dagens datum och ett event
				if(fourWeekThreeDays[i] === self.props.eventDate[j] && self.props.eventDate[j] === moment().format("YYYY-MM-DD")){
					arrEighteen.pop()
					arrEighteen.push(<td className="eventDayToday" key={Math.random()}>{fourWeekThreeDays[i].substr(8,2)}</td>)
				}

			}
		}

		return arrEighteen
	}

	fourWeekFour(fourWeekFourDays){
		var arrNineteen = [];

		for(var i = 0; i < fourWeekFourDays.length; i++){
			
			// Dagens datum
			if(moment().format("YYYY-MM-DD") === fourWeekFourDays[i]){
				arrNineteen.push(<td className="today" key={Math.random()}>{fourWeekFourDays[i].substr(8,2)}</td>)
			}

			// Dag utan event
			else {
				arrNineteen.push(<td className="regularDay" key={Math.random()}>{fourWeekFourDays[i].substr(8,2)}</td>)
			}

			for(var j = 0; j < self.props.eventDate.length; j++){
				
				// Dag med event
				if(fourWeekFourDays[i] === self.props.eventDate[j]){
					arrNineteen.pop()
					arrNineteen.push(<td className="eventDay" key={Math.random()}>{fourWeekFourDays[i].substr(8,2)}</td>)
				}

				// Dagens datum och ett event
				if(fourWeekFourDays[i] === self.props.eventDate[j] && self.props.eventDate[j] === moment().format("YYYY-MM-DD")){
					arrNineteen.pop()
					arrNineteen.push(<td className="eventDayToday" key={Math.random()}>{fourWeekFourDays[i].substr(8,2)}</td>)
				}

			}
		}

		return arrNineteen
	}

	fourWeekFive(fourWeekFiveDays){
		var arrTwenty = [];

		for(var i = 0; i < fourWeekFiveDays.length; i++){
			
			// Dagens datum
			if(moment().format("YYYY-MM-DD") === fourWeekFiveDays[i]){
				arrTwenty.push(<td className="today" key={Math.random()}>{fourWeekFiveDays[i].substr(8,2)}</td>)
			}

			// Dag utan event
			else {
				arrTwenty.push(<td className="regularDay" key={Math.random()}>{fourWeekFiveDays[i].substr(8,2)}</td>)
			}

			for(var j = 0; j < self.props.eventDate.length; j++){
				
				// Dag med event
				if(fourWeekFiveDays[i] === self.props.eventDate[j]){
					arrTwenty.pop()
					arrTwenty.push(<td className="eventDay" key={Math.random()}>{fourWeekFiveDays[i].substr(8,2)}</td>)
				}

				// Dagens datum och ett event
				if(fourWeekFiveDays[i] === self.props.eventDate[j] && self.props.eventDate[j] === moment().format("YYYY-MM-DD")){
					arrTwenty.pop()
					arrTwenty.push(<td className="eventDayToday" key={Math.random()}>{fourWeekFiveDays[i].substr(8,2)}</td>)
				}

			}
		}

		return arrTwenty
	}

		fourWeekSix(fourWeekSixthDays){
		var arrTwentyExtra = [];

		for(var i = 0; i < fourWeekSixthDays.length; i++){
			
			// Dagens datum
			if(moment().format("YYYY-MM-DD") === fourWeekSixthDays[i]){
				arrTwentyExtra.push(<td className="today" key={Math.random()}>{fourWeekSixthDays[i].substr(8,2)}</td>)
			}

			// Dag utan event
			else {
				arrTwentyExtra.push(<td className="regularDay" key={Math.random()}>{fourWeekSixthDays[i].substr(8,2)}</td>)
			}

			for(var j = 0; j < self.props.eventDate.length; j++){
				
				// Dag med event
				if(fourWeekSixthDays[i] === self.props.eventDate[j]){
					arrTwentyExtra.pop()
					arrTwentyExtra.push(<td className="eventDay" key={Math.random()}>{fourWeekSixthDays[i].substr(8,2)}</td>)
				}

				// Dagens datum och ett event
				if(fourWeekSixthDays[i] === self.props.eventDate[j] && self.props.eventDate[j] === moment().format("YYYY-MM-DD")){
					arrTwentyExtra.pop()
					arrTwentyExtra.push(<td className="eventDayToday" key={Math.random()}>{fourWeekSixthDays[i].substr(8,2)}</td>)
				}

			}
		}

		return arrTwentyExtra
	}

	render() {

		self = this



			var firstMonthArr = [];
			var secondMonthArr = [];
			var thirdMonthArr = [];
			var fourthMonthArr = [];


		// ONE - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		
		// Hur många dagar i månaden
		var firstMonth = moment().startOf("month").format("YYYY-MM-DD");
			var daysFirstMonth = moment(firstMonth).daysInMonth();
		
		// Pushar in de tomma rutorna
		for (var i = 1; i < moment().startOf("month").format("E"); i++) {
			firstMonthArr.push("Tom")
		}

		// Pushar in resterande dagar i arrayen
		for(var i = 0; i < daysFirstMonth; i++){
			firstMonthArr.push(moment().startOf("month").add(i, "days").format("YYYY-MM-DD")	)
		}

		// console.log(firstMonthArr)

		// Delar upp månaden i veckor
		var oneFirstWeek = firstMonthArr.splice(0, 7);
		var oneSecondWeek = firstMonthArr.splice(0, 7);
		var oneThirdWeek = firstMonthArr.splice(0, 7);
		var oneFourthWeek = firstMonthArr.splice(0, 7);
		var oneFifthWeek = firstMonthArr.splice(0, 7);
		var oneSixthWeek = firstMonthArr.splice(0, 7);



		// TWO - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

		// Hur många dagar i månaden
		var secondMonth = moment().startOf("month").add(1, "month").format("YYYY-MM-DD");
			var daysSecondMonth = moment(secondMonth).daysInMonth();

		// Pushar in de tomma rutorna
		for (var i=1; i < moment().startOf("month").add(1, "month").format("E"); i++) {
			secondMonthArr.push("Tom")
		}

		// Pushar in resterande dagar i arrayen
		for(var i = 0; i < daysSecondMonth; i++){
			secondMonthArr.push(moment().startOf("month").add(1, "month").add(i, "days").format("YYYY-MM-DD"))
		}

		// Delar upp månaden i veckor
		var twoFirstWeek = secondMonthArr.splice(0, 7);
		var twoSecondWeek = secondMonthArr.splice(0, 7);
		var twoThirdWeek = secondMonthArr.splice(0, 7);
		var twoFourthWeek = secondMonthArr.splice(0, 7);
		var twoFifthWeek = secondMonthArr.splice(0, 7);
		var twoSixthWeek = secondMonthArr.splice(0, 7);

		// THREE - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

		// Hur många dagar i månaden
		var thirdMonth = moment().startOf("month").add(2, "month").format("YYYY-MM-DD");
			var daysThirdMonth = moment(thirdMonth).daysInMonth();

		// Pushar in de tomma rutorna
		for (var i=1; i < moment().startOf("month").add(2, "month").format("E"); i++) {
			thirdMonthArr.push("Tom")
		}

		// Pushar in resterande dagar i arrayen
		for(var i = 0; i < daysThirdMonth; i++){
			thirdMonthArr.push(moment().startOf("month").add(2, "month").add(i, "days").format("YYYY-MM-DD"))
		}

		// Delar upp månaden i veckor
		var threeFirstWeek = thirdMonthArr.splice(0, 7);
		var threeSecondWeek = thirdMonthArr.splice(0, 7);
		var threeThirdWeek = thirdMonthArr.splice(0, 7);
		var threeFourthWeek = thirdMonthArr.splice(0, 7);
		var threeFifthWeek = thirdMonthArr.splice(0, 7);
		var threeSixthWeek = thirdMonthArr.splice(0, 7);

		// FOUR - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		// Hur många dagar i månaden
		var fourthMonth = moment().startOf("month").add(3, "month").format("YYYY-MM-DD");
			var daysFourthMonth = moment(fourthMonth).daysInMonth();

		// Pushar in de tomma rutorna
		for (var i=1; i < moment().startOf("month").add(3, "month").format("E"); i++) {
			fourthMonthArr.push("Tom")
		}

		// Pushar in resterande dagar i arrayen
		for(var i = 0; i < daysFourthMonth; i++){
			fourthMonthArr.push(moment().startOf("month").add(3, "month").add(i, "days").format("YYYY-MM-DD"))
		}

		// Delar upp månaden i veckor
		var fourFirstWeek = fourthMonthArr.splice(0, 7);
		var fourSecondWeek = fourthMonthArr.splice(0, 7);
		var fourThirdWeek = fourthMonthArr.splice(0, 7);
		var fourFourthWeek = fourthMonthArr.splice(0, 7);
		var fourFifthWeek = fourthMonthArr.splice(0, 7);
		var fourSixthWeek = fourthMonthArr.splice(0, 7);

		// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		var calCounter = 0;

		return (

					<div id="calendar-container">
		
					<header className="calendar-head">
						<h1>CALENDAR</h1>
					</header>
					
					<div id="calendar">
					
					<h2 className="month">{moment().format("MMMM")}</h2>
					<table>
						<tbody className="calendarBody">

							<tr>
								<th>Mon</th>
								<th>Tue</th>
								<th>Wed</th>
								<th>Thu</th>
								<th>Fri</th>
								<th>Sat</th>
								<th>Sun</th>
							</tr>

							<tr className="calendarRow">{this.oneWeekOne(oneFirstWeek)}</tr>
							<tr className="calendarRow">{this.oneWeekTwo(oneSecondWeek)}</tr>
							<tr className="calendarRow">{this.oneWeekThree(oneThirdWeek)}</tr>
							<tr className="calendarRow">{this.oneWeekFour(oneFourthWeek)}</tr>
							<tr className="calendarRow">{this.oneWeekFive(oneFifthWeek)}</tr>
							<tr className="calendarRow">{this.oneWeekSix(oneSixthWeek)}</tr>
						</tbody>
					</table>


					<h2 className="month">{moment().add(1, "month").format("MMMM")}</h2>
					<table>
						<tbody>

						<tr>
							<th>Mon</th>
							<th>Tue</th>
							<th>Wed</th>
							<th>Thu</th>
							<th>Fri</th>
							<th>Sat</th>
							<th>Sun</th>
						</tr>
							
							<tr className="calendarRow">{this.twoWeekOne(twoFirstWeek)}</tr>
							<tr className="calendarRow">{this.twoWeekTwo(twoSecondWeek)}</tr>
							<tr className="calendarRow">{this.twoWeekThree(twoThirdWeek)}</tr>
							<tr className="calendarRow">{this.twoWeekFour(twoFourthWeek)}</tr>
							<tr className="calendarRow">{this.twoWeekFive(twoFifthWeek)}</tr>
							<tr className="calendarRow">{this.twoWeekSix(twoSixthWeek)}</tr>
						</tbody>
					</table>

					<h2 className="month">{moment().add(2, "month").format("MMMM")}</h2>
					<table>
						<tbody>
						
							<tr>
								<th>Mon</th>
								<th>Tue</th>
								<th>Wed</th>
								<th>Thu</th>
								<th>Fri</th>
								<th>Sat</th>
								<th>Sun</th>
							</tr>
							
							<tr className="calendarRow">{this.threeWeekOne(threeFirstWeek)}</tr>
							<tr className="calendarRow">{this.threeWeekTwo(threeSecondWeek)}</tr>
							<tr className="calendarRow">{this.threeWeekThree(threeThirdWeek)}</tr>
							<tr className="calendarRow">{this.threeWeekFour(threeFourthWeek)}</tr>
							<tr className="calendarRow">{this.threeWeekFive(threeFifthWeek)}</tr>
							<tr className="calendarRow">{this.threeWeekSix(threeSixthWeek)}</tr>
						</tbody>
					</table>

					<h2 className="month">{moment().add(3, "month").format("MMMM")}</h2>
					<table>
						<tbody>
						
							<tr>
								<th>Mon</th>
								<th>Tue</th>
								<th>Wed</th>
								<th>Thu</th>
								<th>Fri</th>
								<th>Sat</th>
								<th>Sun</th>
							</tr>
							
							<tr className="calendarRow">{this.fourWeekOne(fourFirstWeek)}</tr>
							<tr className="calendarRow">{this.fourWeekTwo(fourSecondWeek)}</tr>
							<tr className="calendarRow">{this.fourWeekThree(fourThirdWeek)}</tr>
							<tr className="calendarRow">{this.fourWeekFour(fourFourthWeek)}</tr>
							<tr className="calendarRow">{this.fourWeekFive(fourFifthWeek)}</tr>
							<tr className="calendarRow">{this.fourWeekSix(fourSixthWeek)}</tr>
						</tbody>
					</table>
					
					</div>
				</div>

		)
	}
}

function mapStateToProps(state){
	return { eventDate: state.eventDate };
}

function mapDispatchToProps(dispatch){
	return bindActionCreators ({ fetchEventDates }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps) ( Calender );