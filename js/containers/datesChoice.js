import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

// Importerar action för att posta "Röstade platser" till API
import { chooseDate } from "../actions/index";
import { fetchEventDetails } from "../actions/index";


import { EventDetails } from "../containers/eventDetails";

// -------------------------------------------------------------------------

export class DatesChoice extends Component {

		constructor(props){
		super(props)
		this.handleDateChange = this.handleDateChange.bind(this)
	}

	// Skickar valt datum till servern/databasen
	handleDateChange(){
		chooseDate(this.props.dateID, this.props.token);
		this.props.refresh();
	}

	// Kontrollerar om användaren har röstat på det alternativet redan
	checkChecked(){
		if(this.props.myDateVote === "1"){
			return true;
		}

		else {
			return false;
		}
	}
	

	render(){

		self=this

		var checked = self.checkChecked();

		return(
		
			<div className="datesVoteHolder">
				
				<div className="datesVoter">
					<label htmlFor="voteForDate"></label>
						<label className="datesCheckbox">
    				<input type="checkbox" name="voteForDate" checked={checked} value={this.props.dateID} onChange={this.handleDateChange.bind(this)} />
   					<span></span>
					</label>
				</div>

				<div className="datesVotes">
					<p>{this.props.dateCount}</p>
				</div>

			</div>

		)
	}
}

	DatesChoice.propTypes = {
  refresh: React.PropTypes.func,
	};