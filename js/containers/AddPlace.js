import React, { Component } from "react";

export class AddPlace extends Component {
	constructor(){
		super()
		this.handleChange = this.handleChange.bind(this);
	}

	// Kallar på metoden "setSelectedPlaceState" i eventDetails som sätter
	/// vald plats i state.
	handleChange(){
		this.props.setTheLocation(this.props.location) 
	}

	render(){

		self=this
	
		return(
			<div className="inputHolder">
				<input className="addPlaceInput" placeholder="Search to add new location..." />
			</div>
		)
	}
}

AddPlace.propTypes = {
	setTheLocation: React.PropTypes.func,
};
