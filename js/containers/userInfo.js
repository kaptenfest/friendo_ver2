import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

// Importerar action för att hämta User Info  från API
import { fetchUserInfo } from "../actions/index.js";


// - - - - - Det här anropet kommer att användas till att bygga upp profilsidan - - - - - 


// FRÅN INLOGG - - - - - - - - - - - - -

// Hämta user_ID från Localstorage
const user_id = localStorage.getItem("user_id")
	
// Hämta token från Localstorage
const token = localStorage.getItem("token")

// - - - - - - - - - - - - - - - - - 


class UserInfo extends Component {
	
	componentWillMount() {
		this.props.fetchUserInfo(token);
	}

	render(){

		self=this

		if(this.props.userInfo[0]) {

			return (
				<div> 
					
					<div className="about">
						<h2>About the host</h2>
					</div>
		
					<div className="flexWrapper">
						<div className="lefty">
							
							<div className="avatarHolder">
								<img className="avatar" src={"http://app.friendoapp.com/" + this.props.userInfo[0].avatar}></img>
							</div>
			
							<div className="contactFirst">
								
								<div className="name">
									<p><strong>{this.props.userInfo[0].name}</strong></p>
								</div>
								<div className="mail">
									<p>{this.props.userInfo[0].email}</p>
								</div>
							</div>
	
						</div>
						
						<div className="righty">	
							
							<div className="phone">
								<p>+46 {this.props.userInfo[0].phone}</p>
							</div>
	
							<div className="location">
								<p>{this.props.userInfo[0].location}</p>
							</div>
	
						</div>
					</div>
				
				</div>
			)
		}
		
		else {
			return(
				<div>
					Loading...
				</div>
			)
		}
	}
}

function mapStateToProps(state){
	return { userInfo: state.userInfo };
}

function mapDispatchToProps(dispatch){
	return bindActionCreators ({ fetchUserInfo }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps) (UserInfo);