import React, { Component } from "react";

export class EditPlace extends Component {
	constructor(){
		super()
		this.handleChange = this.handleChange.bind(this);
	}

	handleChange(){
		this.props.setTheLocation(this.props.location) 
	}


	render(){

		self=this

		return(
		
			<div className="placesVoteHolder">
				
				<div className="placesVoter">
					<img className="del" src="../img/eventDetails/del.png" onClick={() => this.handleChange(this.props.location)}></img>
				</div>

				<div className="placesVotes">
					<p>{this.props.placeCount}</p>
				</div>

			</div>

		)
	
	}
}


EditPlace.propTypes = {
	setTheLocation: React.PropTypes.func,
};