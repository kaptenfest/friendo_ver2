// React
import React, { Component } from "react";

// Redux
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

// Actions
import { fetchEvent } from "../actions/index.js"; // Hämtar EventOverview
import { fetchEventDetails } from "../actions/index"; // Hämtar EventDetails
import { chooseEvent } from "../actions/index"; // Byter status på Coming/Maybe/Not
import { fetchEventAttendees } from "../actions/index"; // Hämtar Attendees
import { closeLocationAction } from "../actions/index"; // "Stänger" omröstningen för plats
import { closeDateAction } from "../actions/index"; // "Stänger" omröstningen för tid
import { delPlaceAction } from "../actions/index"; // Tar bort vald plats
import { addPlaceAction } from "../actions/index"; // Lägger till plats från Geosuggest
import { delDateAction } from "../actions/index"; // Tar bort valt datum
import { addDateAction } from "../actions/index"; // Lägger till vald tid från DateTimePicker/Moment
import { changeTitleAction } from "../actions/index"; // Ändrar titel till inskriven text i "titleInput"
import { changeDescriptionAction } from "../actions/index"; // Ändrar description till inskriven text i textarea
import { cancelEvent} from "../actions/index"; // Tar bort egna events

// Containers/Components
import EventOverview from "../containers/eventOverview";
import EventAttendees from "../containers/eventAttendees";
import EventChoice from "../containers/eventChoice";
import GoogleMap from "../components/google_map";

import UserInfo from "../containers/userInfo"; // ( Kommer endast att användas till Profilsidan )

// Moduler
import Geosuggest from 'react-geosuggest'; // Add place
import moment from 'moment';	// Bibliotek för att hantera tid/datum
import Datetime from 'react-datetime'; // Add date

import { DatesChoice } from "./datesChoice"; // För att rösta på datum
import { PlacesChoice } from "./placesChoice"; // För att rösta på platser
import { ClosePlaces } from "./ClosePlaces";	// För att stänga/besluta platser
import { CloseDate } from "./CloseDate"; // För att stänga/besluta tider
import { EditPlace } from "./EditPlace"; // För att editera platser
import { EditDate } from "./EditDate"; // För att editera tider
import { AddPlace } from "./AddPlace";	// För att lägga till platser i eventDetails


// React Router
import { Router, Route, Link, hashHistory } from 'react-router';


// Från Login - - - - - - - - - - - - -

// Hämta user_ID från Localstorage
const user_id = localStorage.getItem("user_id")
	
// Hämta token från Localstorage
const token = localStorage.getItem("token")

// - - - - - - - - - - - - - - - - - 

// -------------------------------------------------------

class EventDetails extends Component {
	constructor(props){
		super(props)
		this.fresh = this.fresh.bind(this) // All uppdatering av state

		this.showCloseLocation = this.showCloseLocation.bind(this) // Visa Location Omröstning
		this.hideCloseLocation = this.hideCloseLocation.bind(this) // Göm Location Omröstning

		this.showCloseDate = this.showCloseDate.bind(this) // Visa Date Omröstning
		this.hideCloseDate = this.hideCloseDate.bind(this) // Göm Date Omröstning

		this.showEditLocation = this.showEditLocation.bind(this) // Visa Location Edit (ta bort platser)
		this.hideEditLocation = this.hideEditLocation.bind(this) // Göm Location Edit (ta bort platser)

		this.showEditDate = this.showEditDate.bind(this) // Visa Date Edit (ta bort tider)
		this.hideEditDate = this.hideEditDate.bind(this) // Göm Date Edit (ta bort tider)
		
		this.setPlaceState = this.setPlaceState.bind(this) // Vald location för att stänga omröstning till state
		this.postToServer = this.postToServer.bind(this) // Skickar vald location för att stänga omröstningen till databasen

		this.setDateState = this.setDateState.bind(this) // Valt date för att stänga omröstning till state
		this.postDateToServer = this.postDateToServer.bind(this) // Skickar valt date för att stänga omröstningen till databasen

		this.setSelectedPlaceState = this.setSelectedPlaceState.bind(this) // Vald location att editera till state
		this.filteredLocation = this.filteredLocation.bind(this) // Filtrerar ut allt som inte är den valda location att editera 
		
		this.fromGeoSuggest = this.fromGeoSuggest.bind(this) // Info från geosuggest och sätter det i state
		this.fromLocations = this.fromLocations.bind(this) // Info från existerande location från reduxstate/databasen

		this.setSelectedDateState = this.setSelectedDateState.bind(this); // Valt date att editera till state (Inte implementerad ännu)
		this.filteredDate = this.filteredDate.bind(this); // Valt date att editera till state (Inte implementerad ännu)

		this.addStartDate = this.addStartDate.bind(this); // Sätter valt startDate till state
		this.addEndDate = this.addEndDate.bind(this); // Sätter valt startDate till state

		this.showEditTitle = this.showEditTitle.bind(this); // Visar Input fältet
		this.hideEditTitle = this.hideEditTitle.bind(this); // Visar Title / Gömmer Input
		this.setUpdatedTitle = this.setUpdatedTitle.bind(this); // Sätter inskriven title till state

		this.showEditDescription = this.showEditDescription.bind(this) // Visar "edit textarea" för description
		this.hideEditDescription = this.hideEditDescription.bind(this) // Gömmer "edit textarea" för description
		this.setUpdatedDescription = this.setUpdatedDescription.bind(this) // Sätter inskriven description till state

		this.adminCheckFunction = this.adminCheckFunction.bind(this) // Kollar om user === hostid - Location

		this.showCancelEvent = this.showCancelEvent.bind(this); // Visa Cancel Event
		this.hideCancelEvent = this.hideCancelEvent.bind(this); // Dölj Cancel Event



		this.state = {
			checkPlace: "show", // Locations - rösta på locations (div)
			radioPlace: "hide",	// Locations - stänga omröstningen (div)
			editPlace: "hide",	// Locations - ta bort/lägga till locations (div)
			activePlaceState: "", // Locations - lagrar vald plats för att stänga omröstningen 
			selectedPlaceState: [""], // Locations - lagrar info om vald location för att ta bort
			delPlace: "hide", // Locations - ta bort location (div)
			addedPlace: [], // Locations - lagrar vald plats att lägga till

			checkDate: "show", // Dates - rösta på dates (div)
			radioDate: "hide", // Dates - stänga omröstningen (div)
			editDate: "hide", // Dates - ta bort/lägga till dates (div)
			activeDateState: "",	// Dates - lagrar vald date för att stänga omröstningen 
			selectedDateState: [""], // Dates - lagrar info om vald date för att ta bort
			delDate: "hide", // Dates - ta bort date (div)

			inputTitle: "hide", // Gömmer input för att ändra Title;
			title: "show", // Visar / Gömmer title
			updatedTitle: "", // Det som skrivs in i edit Title input
			
			addedStart: "", // Tillagd starttid i UNIX time
			addedEnd: "", // Tillad sluttid i UNIX time

			editDescription: "hide", // Gömmer textarea för att ändra description
			eventDescription: "show", // Visar Event Description
			updatedDescription: "", // Det som skrivs in i edit Description textarea

			confirmCancelEvent: "hide", // Kontrollerar om du vill "ta bort" event

			adminCheck: "", // Visar och gömmer alla edit länkar beroende på om man är "admin" eller inte

			coming: "hide", // Pop-up för attending
			maybe: "hide",	// Pop-up för maybe attending
			not: "hide"	// Pop-up för not attending
			
		}

		var counter = 0; // Håller koll på vilket nr som ska stå på location markers.

	}

	componentWillMount() {
		this.props.fetchEventDetails(this.props.params.theEventID, token); // Laddar Event Details för valt event
	}

	// Kontrollerar om man har rätt att editera event
	componentDidUpdate(){
		this.adminCheckFunction();	
	}


	// Uppdaterar redux-state för Event Overview / Event Details
	fresh(){
		this.props.fetchEvent(user_id); // Event Overview
		this.props.fetchEventDetails(this.props.eventDetails[0].id, token); // Event Details
	}


	// Kommer / Kommer kanske / Kommer inte Dropdown
	statusChange(){
		var getSelected = document.getElementById('selecter' + this.props.eventDetails[0].id);
		var selectedValue = getSelected.value; // Valet
		var eventID = this.props.eventDetails[0].id; // EventID:et
		setTimeout(() => {
			chooseEvent(selectedValue, eventID, token); // Choose Event Action
			this.fresh(); // Uppdaterar state för Event Overview / Event Details
			this.updateAttendees(eventID); // Uppdatera state för Attendees på det valda Event Details.
		}, 100)
		this.closeAttendingList();
	}


	// Hämtar attendees och uppdaterar redux-state för Event Overview / Event Details
	updateAttendees(theEventAJD){
		setTimeout(() => {
			this.fresh(); // Event Overview / Event Details
			this.props.fetchEventAttendees(theEventAJD) // Hämtar Attendees
		}, 100)
	}


	// Rendrerar location vid öppen röstning (med gömd stäng omröstning & lägg till/ta bort)
	// @param voteForLocation - mappar över locations arrayen
	// @param counter - nr för markers
	locationVote(voteForLocation, counter){

			if (self.props.eventDetails[0].locationSettle === ""){

				counter++

			// Kort address/namn
			var shorted = voteForLocation.values;
			shorted = shorted.substring(0, shorted.indexOf('§') );

			// Resten av addressen
			var rested = voteForLocation.values;
			rested = rested.substring(rested.indexOf('§') + 1 )

			return (
			<div key={voteForLocation.id}>
			
			<div className={self.state.checkPlace}>
				<div className="locationHolder">
					
					<div className="placeHolder">
						<div className="placesTitle">
							<p><span className="counter">{counter}</span> {shorted}</p>
							<p></p>
						</div>
						<div className="placesDetailed">
							<p>{rested}</p>
						</div>
					</div>

					<PlacesChoice 
						placeID={voteForLocation.id} 
						placeCount={voteForLocation.count} 
						myPlaceVote={voteForLocation.uVote} 
						refresh={self.fresh} 
						token={token}
						/>
				
				</div>

			</div>

			<div className={self.state.radioPlace}>
				<div className="closeLocationHolder">
		
					<div className="closeLocationPlaceholder">
						<div className="closeLocationPlacesTitle">
							<p><strong>{shorted}</strong></p>
						</div>
						<div className="closeLocationPlacesDetailed">
							<p>{rested}</p>
						</div>
					</div>
		
					<ClosePlaces 
						placeID={voteForLocation.id} 
						placeCount={voteForLocation.count}
						eventID={self.props.eventDetails[0].id}
						closeTheLocation={self.setPlaceState}
						refresh={self.fresh} />
						
				</div>
			</div>

			<div className={self.state.editPlace}>
				<div className="closeLocationHolder">
		
					<div className="closeLocationPlaceholder">
						<div className="closeLocationPlacesTitle">
							<p><strong>{shorted}</strong></p>
						</div>
						<div className="closeLocationPlacesDetailed">
							<p>{rested}</p>
						</div>
						
					</div>
		
					<EditPlace
						location={voteForLocation}
						setTheLocation={self.setSelectedPlaceState}
						placeCount={voteForLocation.count} />
				</div>
			</div>

			</div>

			)
		}
	}


	// Rendrerar location vid stängd röstning
	// @params locationsDetails - mappar över locations
	locationSet(locationDetails){
		if (locationDetails.id === self.props.eventDetails[0].locationSettle){
			
			// Kort address/namn
			var shorted = locationDetails.values;
			shorted = shorted.substring(0, shorted.indexOf('§') );

			// Resten av addressen
			var rested = locationDetails.values;
			rested = rested.substring(rested.indexOf('§') + 1 )
				
			return (
				<div key={locationDetails.id}>

				<h2>Location</h2>
					<div className="locationHolder">
						<div className="placeHolder">
							<div className="placesTitle">
								<p><span className="settled_pointer">&#9898;</span>{shorted}</p>
							</div>
							<div className="placesDetailed">
								<p>{rested}</p>
							</div>
						</div>
					</div>

				</div>
			)
		}
	}


	// Visar endast "Edit Location" om det är den inloggade användaren som är host för eventet
	adminCheckFunction(){
		if(user_id === this.props.eventDetails[0].userid && this.state.adminCheck === "" || user_id === this.props.eventDetails[0].userid && this.state.adminCheck === "hide"){
			this.setState({adminCheck: "show"})
		}
		if(user_id != this.props.eventDetails[0].userid && this.state.adminCheck === "" || user_id != this.props.eventDetails[0].userid && this.state.adminCheck === "show") {
			this.setState({adminCheck: "hide"})
		}
	}
	

	// Rendrerar dates vid öppen röstning (med gömd stäng omröstning & lägg till/ta bort)
	// @params voteForTime - mappar över starttiderna
	// return time - ex. 01 Januari, 12.00
	// return time2 - ex. 01 Januari, 12.00
	timeVote(voteForTime){
		if (self.props.eventDetails[0].startimeSettle === ""){

		// Konvertera UNIX tid / Starttid
		var a = new Date(Number(voteForTime.values));
 		var months = ['January','February','Mars','April','May','June','July','August','September','October','November','December'];
 		var month = months[a.getMonth()];
 		var date = self.addZero(a.getDate());
 		var hour = self.addZero(a.getHours());
 		var min = self.addZero(a.getMinutes());
 		var time = date + ' ' + month + ', ' + hour + ':' + min;

 		// Konvertera UNIX tid / Sluttid
 		var b = new Date(Number(voteForTime.endtime));
 		var months2 = ['January','February','Mars','April','May','June','July','August','September','October','November','December'];
 		var month2 = months[b.getMonth()];
 		var date2 = self.addZero(b.getDate());
 		var hour2 = self.addZero(b.getHours());
 		var min2 = self.addZero(b.getMinutes());
 		var time2 = date2 + ' ' + month2 + ', ' + hour2 + ':' + min2;


 		// Rendrerar olika beroende på om det finns en sluttid eller inte
 		// @params voteForTime - mappar över tiderna
 		function checkEndtime(voteForTime){
 			if(voteForTime.endtime === ""){
 				return (<p className="timeHolder2">{time} - No endtime</p>)
 			}
 			else {
 				return (<p className="timeHolder2">{time} - {time2}</p>)
 			}
 		}

		return (
		<div key={voteForTime.id}>
			<div className={self.state.checkDate}>
				<div className="dateHolder">
					<div className="timeHolder">
						{checkEndtime(voteForTime)}
					</div>

					<DatesChoice 
					dateID={voteForTime.id} 
					dateCount={voteForTime.count} 
					myDateVote={voteForTime.uVote} 
					anEventID={self.props.params.theEventID} 
					refresh={self.fresh} 
					token={token}
					/>

				</div>
			</div>

			<div className={self.state.radioDate}>
				<div className="closeDateHolder">
					<div className="closeTimeHolder">
						{checkEndtime(voteForTime)}
					</div>

					<CloseDate 
					dateID={voteForTime.id} 
					dateCount={voteForTime.count} 
					myDateVote={voteForTime.uVote} 
					anEventID={self.props.params.theEventID} 
					closeTheDate={self.setDateState}
					refresh={self.fresh} />

				</div>
			</div>

			<div className={self.state.editDate}>
				<div className="closeDateHolder">
					<div className="closeTimeHolder">
						{checkEndtime(voteForTime)}
					</div>

					<EditDate 
						date={voteForTime}
						setTheDate={self.setSelectedDateState}
						dateCount={voteForTime.count} />

				</div>
			</div>

		</div>

		)

		}
	}


	// Dates vid stängd röstning
	// @param timeDetails - mappar över starttiderna
	// return time - ex. 01 Januari, 12.00
	// return time2 - ex. 01 Januari, 12.00
	timeSet(timeDetails){
		if (timeDetails.id === self.props.eventDetails[0].startimeSettle){

			// Konvertera UNIX tid / Starttid
			var a = new Date(Number(timeDetails.values));
 			var months = ['January','February','Mars','April','May','June','July','August','September','October','November','December'];
 			var month = months[a.getMonth()];
 			var date = self.addZero(a.getDate());
 			var hour = self.addZero(a.getHours());
 			var min = self.addZero(a.getMinutes());
 			var time = date + ' ' + month + ', ' + hour + ':' + min;

 			// Konvertera UNIX tid / Sluttid
 			var b = new Date(Number(timeDetails.endtime));
 			var months2 = ['January','February','Mars','April','May','June','July','August','September','October','November','December'];
 			var month2 = months[b.getMonth()];
 			var date2 = self.addZero(b.getDate());
 			var hour2 = self.addZero(b.getHours());
 			var min2 = self.addZero(b.getMinutes());
 			var time2 = date2 + ' ' + month2 + ', ' + hour2 + ':' + min2;

			return (
				<div key={timeDetails.id}>
				<h2>Date/Time</h2>
					<div className="dateHolder">
						<div className="timeHolder">
							<p className="timeHolder2">{time} - {time2}</p>
						</div>
					</div>
				</div>
			)
		}
	}

	// Lägg till en nolla på datum/tid fix
	addZero(i) {
  	if (i < 10) {
  	 	i = "0" + i;
  	}
  	return i;
	}


	/* VISA OCH GÖM ELEMENT */

	// LOCATION
	showCloseLocation(){
		this.setState({checkPlace: "hide"}); // Göm - Rösta på locations
		this.setState({radioPlace: "show"}); // Visa - Stäng omröstningen för locations
	}

	hideCloseLocation(){
		this.setState({checkPlace: "show"}); // Visa - Rösta på locations
		this.setState({radioPlace: "hide"}); // Göm - Stäng omröstningen för locations
	}

	showEditLocation(){
		this.setState({checkPlace: "hide"}); // Göm - Rösta på locations
		this.setState({radioPlace: "hide"}) // Göm - Stäng omröstningen för locations
		this.setState({editPlace: "show"}) // Visa - Ta bort/Lägg till locations
	}

	hideEditLocation(){
		this.setState({checkPlace: "show"}) // Visa - Rösta på locations
		this.setState({radioPlace: "hide"}) // Göm - Stäng omröstningen för locations
		this.setState({editPlace: "hide"}) // Göm - Ta bort/Lägg till locations
		this.setState({delPlace: "hide"}) // Göm - Ta bort location
	}

	// DATE
	showCloseDate(){
		this.setState({checkDate: "hide"}); // Göm - Rösta på dates
		this.setState({radioDate: "show"}); // Visa - Stäng omröstningen för dates
	}

	hideCloseDate(){
		this.setState({checkDate: "show"}); // Visa - Rösta på dates
		this.setState({radioDate: "hide"}); // Göm - Stäng omröstningen för dates
	}

	showEditDate(){
		this.setState({checkDate: "hide"}); // Göm - Rösta på dates
		this.setState({radioDate: "hide"}); // Göm - Stäng omröstningen för dates
		this.setState({editDate: "show"}); // Visa - Ta bort/Lägg till dates
	}

	hideEditDate(){
		this.setState({checkDate: "show"}) // Visa - Rösta på dates
		this.setState({radioDate: "hide"}) // Göm - Stäng omröstningen för dates
		this.setState({editDate: "hide"}) // Göm - Ta bort/Lägg till dates
		this.setState({delDate: "hide"}) // Göm - Ta bort date
	}

	// CANCEL EVENT
	showCancelEvent(){
		this.setState({confirmCancelEvent: "show"})
		this.setState({cancelButton: "hide"})
	}

	hideCancelEvent(){
		this.setState({confirmCancelEvent: "hide"})
		this.setState({cancelButton: "show"})
	}


	// STÄNG OMRÖSTNING FÖR LOCATION //

	// Vald location för att stänga omröstning till stat
	// @param activePlace - aktiv plats från ClosePlaces.js
	setPlaceState(activePlace){
		this.setState({activePlaceState: activePlace})
	}

	// Skickar vald plats till databasen för att stänga omröstningen
	// @param place - aktiv plats
	postToServer(place){
		closeLocationAction(place, this.props.eventDetails[0].id, token)
			this.fresh();
	}

	cancelEventAction(){
		cancelEvent(this.props.eventDetails[0].id, user_id);
	}


	// STÄNG OMRÖSTNING FÖR DATE //

	// Vald date för att stänga omröstning till state
	// @param activeDate - aktiv tid från CloseDates.js
	setDateState(activeDate){
		this.setState({activeDateState: activeDate})
	}

	// Skickar valt date till databasen för att stänga omröstningen
	// @param date - aktivt datum
	postDateToServer(date){
		closeDateAction(date, this.props.eventDetails[0].id, token)
			this.fresh();
	}


	// Location - Headers för att stänga omröstning / Lägga till / Ta bort
	setLocationHeaders(){
		if (self.props.eventDetails[0].locationSettle === "") {

			if(self.state.selectedPlaceState.values === undefined){
				var shorted = ""
			}

			else{
				var shorted = self.state.selectedPlaceState.values;
				shorted = shorted.substring(0, shorted.indexOf('§') );	
			}
			
			return ( 
				<div>
				
				{/* Öppen röstning - Location */}
					<div className="endVotingHeader">
						<div><h2 className={self.state.checkPlace}>Location - Voting in progress...</h2></div>
						<div className={self.state.adminCheck}><h2 className={self.state.checkPlace}><a onClick={self.showCloseLocation}>Admin</a></h2></div>
						<div className={self.state.adminCheck}><h2 className={self.state.checkPlace}><a id="alignRight" onClick={self.showEditLocation}>Edit</a></h2></div>
					</div>
				
				{/* Stäng röstning */}
				<div className="endVotingHeader">
					<div><h2 className={self.state.radioPlace}>{this.isCloseLocationSet()}</h2></div>
					<div><h2 className={self.state.radioPlace}><a onClick={self.hideCloseLocation} className="cancel">Cancel</a></h2></div>
				</div>
	
				{/* Editera locations */}
				<div className="editHeader">
					<div><h2 className={self.state.editPlace}>Edit Location</h2></div>
					<div><h2 className={self.state.editPlace}><a onClick={self.hideEditLocation} className="cancel">Cancel</a></h2></div>
				</div>
	
				{/* Ta bort location */}
				<div className="box">
					<div className="confirmLocation"><h2 className={self.state.delPlace}>Delete {shorted}?</h2></div>
					<div><h2 className={self.state.delPlace}><a className="confirm" onClick={() => this.delSelectedPlace()}>Confirm</a> &nbsp; <a onClick={self.hideEditLocation} className="cancel">Cancel</a></h2></div>
				</div>
	
				</div>
			)
		}
	}

	// Date - Headers för att stänga omröstning / Lägga till / Ta bort
	setDateHeaders(){
		if (self.props.eventDetails[0].startimeSettle === "") {

		
		// Konvertera UNIX tid / Starttid
		var a = new Date(Number(this.state.selectedDateState.values));
 		var months = ['January','February','Mars','April','May','June','July','August','September','October','November','December'];
 		var month = months[a.getMonth()];
 		var date = self.addZero(a.getDate());
 		var hour = self.addZero(a.getHours());
 		var min = self.addZero(a.getMinutes());
 		var time = date + ' ' + month + ', ' + hour + ':' + min;

 		// Konvertera UNIX tid / Sluttid
 		
 		var b = new Date(Number(this.state.selectedDateState.endtime));
 		var months2 = ['January','February','Mars','April','May','June','July','August','September','October','November','December'];
 		var month2 = months[b.getMonth()];
 		var date2 = self.addZero(b.getDate());
 		var hour2 = self.addZero(b.getHours());
 		var min2 = self.addZero(b.getMinutes());
 		var time2 = date2 + ' ' + month2 + ', ' + hour2 + ':' + min2;


 		// Rendrerar olika beroende på om det finns en sluttid eller inte
 		// @params voteForTime - mappar över tiderna
 		function checkEndtime(checkDelTime){
 			if(checkDelTime.endtime === ""){
 				return (<p>Delete {time} - No endtime</p>)
 			}
 			else {
 				return (<p>Delete {time} - {time2}?</p>)
 			}
 		}
 		

			return (
			<div>
			
			{/* Öppen röstning - Date/Time */}
				<div className="endVotingHeader">
					<div><h2 className={self.state.checkDate}>Date/Time - Voting in progress...</h2></div>
					<div className={self.state.adminCheck}><h2 className={self.state.checkDate}><a onClick={self.showCloseDate}>Admin</a></h2></div>
					<div className={self.state.adminCheck}><h2 className={self.state.checkDate}><a id="alignRight" onClick={self.showEditDate}>Edit</a></h2></div>
				</div>
				
			{/* Stäng röstning */}
			<div className="endVotingHeader">
				<div><h2 className={self.state.radioDate}>{this.isCloseDateSet()}</h2></div>
				<div><h2 className={self.state.radioDate}><a onClick={self.hideCloseDate} className="cancel">Cancel</a></h2></div>
			</div>

			{/* Editera dates */}
			<div className="editHeader">
				<div><h2 className={self.state.editDate}>Edit Date/Time</h2></div>
				<div><h2 className={self.state.editDate}><a onClick={self.hideEditDate} className="cancel">Cancel</a></h2></div>
			</div>

			{/* Ta bort date */}
			<div className="box">
				<div className="confirmDate"><h2 className={self.state.delDate}>{checkEndtime(this.state.selectedDateState)}</h2></div>
				<div><h2 className={self.state.delDate}><a className="confirm" onClick={() => this.delSelectedDate()}>Confirm</a> &nbsp; <a onClick={self.hideEditDate} className="cancel">Cancel</a></h2></div>
			</div>

			</div>
			)
		}
	}

	// Kontrollerar om en location radiobutton är iklickad
	isCloseLocationSet(){
		if (this.state.activePlaceState === "") {
			return <p className="disable">Choose a location to settle the vote... &nbsp;</p>
		}
		else {
			return <a className="confirm" onClick={() => this.postToServer(this.state.activePlaceState)}>Confirm</a>
		}
	}

	// Kontrollerar om en date radiobutton är iklickad
	isCloseDateSet(){
		if (this.state.activeDateState === "") {
			return <p className="disable">Choose a Date/Time to settle the vote... &nbsp;</p>
		}
		else {
			return <a className="confirm" onClick={() => this.postDateToServer(this.state.activeDateState)}>Confirm Date/Time &nbsp;</a>
		}
	}


	// Google Maps default longitude eller första location i arrayen	
	getDefaultLongitude(){
		for(var i = 0; i < this.props.eventDetails[0].location.length; i++){	

			if(this.props.eventDetails[0].location.length === 0 || this.props.eventDetails[0].location[i].longitude === ""){
				return Number("18.068581");
			}
			
			else {
				return Number(this.props.eventDetails[0].location[0].longitude);
			}

		}
	}


	// Google Maps default latitude eller första location i arrayen
	getDefaultLatitude(){
		for(var i = 0; i < this.props.eventDetails[0].location.length; i++){
			
			if(this.props.eventDetails[0].location.length === 0 || this.props.eventDetails[0].location[i].latitude === ""){
				return Number("59.329323");
			}
			
			else {
				return Number(this.props.eventDetails[0].location[0].latitude);
			}

		}
	}


	// Markera alla locations med markers eller bara en på settled location
	getMarkers(){
		if(this.props.eventDetails[0].locationSettle === ""){
			return this.props.eventDetails[0].location
		}
		
		else {
			return this.props.eventDetails[0].location.filter(this.getSettledMarker)
		}
	}
	
	// @param settled - hämtar bestämt datum
	getSettledMarker(settled){
		return self.props.eventDetails[0].locationSettle === settled.id
		}

	
	// @param selectedPlace - Vald plats från del location i state
	setSelectedPlaceState(selectedPlace){
		this.setState({selectedPlaceState: selectedPlace}) // Vald location
		this.setState({delPlace: "show"})	// Visa - del location
		this.setState({editPlace: "show"}) // Göm - Ta bort/Lägg till locations
	}

	// @param selectedDate - Vald plats från del date i state
	setSelectedDateState(selectedDate){
		this.setState({selectedDateState: selectedDate}) // Valt date
		this.setState({delDate: "show"}) // Visa - del date
		this.setState({editDate: "show"}) // Göm - Ta bort/Lägg till dates
	}

	// Skicka vald location till delPlaceAction + göm/visa & uppdatera EventOverview/EventDetails
	// delPlaceAction(name, start_time, location, image, description, limitperson, id, token)
	delSelectedPlace(){
		setTimeout(() => {
			delPlaceAction(this.props.eventDetails[0].name, this.props.eventDetails[0].start_time.map(this.startTimeString), this.props.eventDetails[0].location.filter(this.filteredLocation).map(this.locationString), this.props.eventDetails[0].image, this.props.eventDetails[0].description, this.props.eventDetails[0].limitperson, this.props.eventDetails[0].id, token);
			this.setState({delPlace: "hide"}) // Göm - del date
			this.setState({editPlace: "show"}) // Visa - Ta bort/Lägg till locations
			this.fresh(); // Uppdatera Event Overview/Event Details
		}, 100)
	}

	// Skicka vald date till delDateAction + göm/visa & uppdatera EventOverview/EventDetails
	// delDateAction(name, start_time, location, image, description, limitperson, id, token)
	delSelectedDate(){
		setTimeout(() => { 
			delDateAction(this.props.eventDetails[0].name, this.props.eventDetails[0].start_time.filter(this.filteredDate).map(this.startTimeString), this.props.eventDetails[0].location.map(this.locationString), this.props.eventDetails[0].image, this.props.eventDetails[0].description, this.props.eventDetails[0].limitperson, this.props.eventDetails[0].id, token);
			this.setState({delDate: "hide"})
			this.setState({editDate: "show"})
			this.fresh();
		}, 100)
	}


	// Fixar till start_time till en string att skicka in till editEvent
	// @param startString - mappar över filtrerad starttid
	startTimeString(startString){
		if(startString.endtime === ""){
			return "{" + `"id":"${startString.id}", "values": "${startString.values}", "endtime": ""` + "}	";
		}
		else{
			return "{" + `"id":"${startString.id}", "values": "${startString.values}", "endtime": "${startString.endtime}"` + "}	";
		}
	}

	// Fixar till location till en string att skicka in till editEvent
	// @param locationsString - mappar över filtrerad location
	locationString(locationsString){
		return "{" + `"id":"${locationsString.id}", "values": "${locationsString.values}", "latitude":"${locationsString.latitude}", "longitude":"${locationsString.longitude}"` + "}";
	}

	// Filtrerar ut allt som inte är den valda location
	filteredLocation(filterLocation){
		return filterLocation != this.state.selectedPlaceState;
	}

	// Filtrerar ut allt som inte är den valda date
	filteredDate(filterDate){
		return filterDate != this.state.selectedDateState;
	}

	// Locations från existerande react-redux state
	// @param loco - mappar över locations 
	fromLocations(loco){
		return "{" + `"id":"${loco.id}", "values": "${loco.values}", "latitude":"${loco.latitude}", "longitude":"${loco.longitude}"` + "}";
	}

	// Info från Geosuggest som är formaterad och skickas in till addPlace action
	/// @param suggest - response från GeoSuggest modulen
	fromGeoSuggest(suggest){
		var str = suggest.gmaps.formatted_address;
		if (str.match(/.*,/)) { 
   		str = str.replace(',', '§');
   	}

		if(this.props.eventDetails[0].location.map(this.fromLocations).length === 0){
			this.setState({addedPlace: this.props.eventDetails[0].location.map(this.fromLocations) + "{" + `"values": "${str}", "latitude":"${suggest.location.lat}", "longitude":"${suggest.location.lng}"` + "}"})
		}
		else {
			this.setState({addedPlace: this.props.eventDetails[0].location.map(this.fromLocations) + ",{" + `"values": "${str}", "latitude":"${suggest.location.lat}", "longitude":"${suggest.location.lng}"` + "}"})
		}
		
		setTimeout(() => {
			addPlaceAction(this.props.eventDetails[0].name, this.props.eventDetails[0].start_time.map(this.startTimeString), this.state.addedPlace, this.props.eventDetails[0].image, this.props.eventDetails[0].description, this.props.eventDetails[0].limitperson, this.props.eventDetails[0].id, token)
			this.fresh();
		}, 100)

	}


	/* DATE AND TIME PICKER - - - - - - - - - - - - - - - - */

	fromDatePicker(){
		if(this.state.addedStart < Date.now() && document.getElementsByClassName('form-control')[0].value != "" ){
			document.getElementsByClassName('form-control')[0].value = "Startdate have passed...";
		}
		else if(this.state.addedEnd < Date.now()){
			document.getElementsByClassName('form-control')[1].value = "Enddate have passed...";
		}
		else {
			setTimeout(() => {
			addDateAction(this.props.eventDetails[0].name, this.props.eventDetails[0].start_time.map(this.startTimeString) + ",{" + `"id":"0", "values": "${this.state.addedStart}", "endtime": "${this.state.addedEnd}"` + "}", this.props.eventDetails[0].location.map(this.fromLocations), this.props.eventDetails[0].image, this.props.eventDetails[0].description, this.props.eventDetails[0].limitperson, this.props.eventDetails[0].id, token)
			this.fresh();
			})
		}

	}

	  renderDay ( props, currentDate, selectedDate ){
   		return <td {...props}>{ currentDate.date() }</td>;
    }

    renderMonth( props, month, year, selectedDate){
    	return <td {...props}>{ month }</td>;
    }

    renderYear( props, year, selectedDate ){
    	return <td {...props}>{ year % 100 }</td>;
    }

    addStartDate(moment){
    	this.setState({addedStart: moment.unix() * 1000 });
    }

    addEndDate(moment){
    	this.setState({addedEnd: moment.unix() * 1000 });
    }

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - */

/* EDIT TITLE  - - - - - - - - - - - - - - - - */

// Visar input för Title
showEditTitle(){
	this.setState({inputTitle: "show"});
	this.setState({title: "hide"});
}

// Gömmer input för Title
hideEditTitle(){
	this.setState({inputTitle: "hide"});
	this.setState({title: "show"});
}

// Hämtar value från input // Sätter state // Skickar action för att ändra titel // Gömmer input för title
setUpdatedTitle(){
	let updateTitle = document.getElementsByClassName("titleInput")[0].value;
	this.setState({updatedTitle: updateTitle})

  setTimeout(() => {
  	changeTitleAction(this.state.updatedTitle, this.props.eventDetails[0].start_time.map(this.startTimeString), this.props.eventDetails[0].location.map(this.fromLocations), this.props.eventDetails[0].image, this.props.eventDetails[0].description, this.props.eventDetails[0].limitperson, this.props.eventDetails[0].id, token)
  	this.fresh();
	}, 100)

	this.hideEditTitle();
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - */

/* EDIT DESCRIPTION  - - - - - - - - - - - - - - - - */

showEditDescription(){
	this.setState({editDescription: "show"})
	this.setState({eventDescription: "hide"})
}

hideEditDescription(){
	this.setState({editDescription: "hide"})
	this.setState({eventDescription: "show"})
}

setUpdatedDescription(){
	let updateDescription = document.getElementsByClassName("descriptionTextarea")[0].value;
	this.setState({updatedDescription: updateDescription})

	setTimeout(() => {
		changeDescriptionAction(this.props.eventDetails[0].name, this.props.eventDetails[0].start_time.map(this.startTimeString), this.props.eventDetails[0].location.map(this.fromLocations), this.props.eventDetails[0].image, this.state.updatedDescription, this.props.eventDetails[0].limitperson, this.props.eventDetails[0].id, token)
		this.fresh();
	}, 100)

	this.hideEditDescription();
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - */


// Pop-up som visas när man klickar på attending stats
come(){
	if(this.props.eventDetails[0].responses.join === 0){
		this.setState({maybe: "hide"})
		this.setState({coming: "hide"})
		this.setState({not: "hide"})
	}
	else {
		this.setState({coming: "show"})
		this.setState({maybe: "hide"})
		this.setState({not: "hide"})
	}
}

maybe(){
	if(this.props.eventDetails[0].responses.can_join === 0){
		this.setState({maybe: "hide"})
		this.setState({coming: "hide"})
		this.setState({not: "hide"})
	}
	else {
		this.setState({maybe: "show"})
		this.setState({coming: "hide"})
		this.setState({not: "hide"})
	}
}

not(){
	if(this.props.eventDetails[0].responses.un_join === 0){
		this.setState({maybe: "hide"})
		this.setState({coming: "hide"})
		this.setState({not: "hide"})
	}
	else {
		this.setState({maybe: "hide"})
		this.setState({coming: "hide"})
		this.setState({not: "show"})
	}
}

closeAttendingList(){
	this.setState({coming: "hide"})
	this.setState({maybe: "hide"})
	this.setState({not: "hide"})
}


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - */

	render(){

		/* Inline styles */
		var join = {
			color: '#8ecc20',
		};

		var canJoin = {
			color: '#f0b71f',
		};

		var unJoin = {
			color: '#b53027',
		};

		self=this

		if(this.props.eventDetails[0]) {

			// @import EventAttendees - skickar över vilka som kommer och inte till eventAttendees.js
			
			return ( 
				<div>

					<div className="header">
						<h1 className="back"><Link to={'/'}>Back</Link></h1>
						<h1 className={this.state.title} id="title"><p>{this.props.eventDetails[0].name}</p><a id="alignRight" className={self.state.adminCheck} onClick={self.showEditTitle}>Edit</a></h1>
						<h1 className={this.state.inputTitle}><input className="titleInput" placeholder={this.props.eventDetails[0].name}></input> &nbsp; <a className="confirm" onClick={self.setUpdatedTitle}>Confirm</a> &nbsp; <a className="cancel" onClick={self.hideEditTitle}>Cancel</a></h1>
						<h1 className="invite"><a href="#">Invite People</a></h1>
					</div>

					<div className="scroll">
					
					<div className="attendees">
						<div className="statusStats">
							<span className="coming" onClick={() => this.come()}>
								<h1>{this.props.eventDetails[0].responses.join}</h1>
								<p>Attending</p>
							</span>
							<span className="maybe" onClick={() => this.maybe()}>
								<h1>{this.props.eventDetails[0].responses.can_join}</h1>
								<p>Maybe</p>
							</span>
							<span className="notComing" onClick={() => this.not()}>
								<h1>{this.props.eventDetails[0].responses.un_join}</h1>
								<p>Not Attending</p>
							</span>
							<span className="invited">
								<h1>{this.props.eventDetails[0].invited.length}</h1>
								<p>Invited</p>
							</span>
						</div>
							
							<EventAttendees realEvent={this.props.params.theEventID} coming={this.state.coming} maybe={this.state.maybe} not={this.state.not} closeList={this.closeAttendingList.bind(this)}/>

							<div className="eventDetailsChooser">	
								<select value = {this.props.eventDetails[0].status} className = {this.props.eventDetails[0].status} id={'selecter' + this.props.eventDetails[0].id} onChange={this.statusChange.bind(this)}>
									<option value="no_join" disabled hidden>Not Answered</option>
									<option style={join} value="join">Attending</option>
									<option style={unJoin} value="un_join">Not Attending</option>
									<option style={canJoin} value="can_join">Maybe Attending</option>
								</select>
							</div>

					</div>

					<div className="eventDetailsLayout">
						
						<div className="eventDetailsLeft">
							
							<div className="eventDescription">
								<img className="eventImage" src={"http://app.friendoapp.com/" + this.props.eventDetails[0].image}></img>

								<div className={this.state.eventDescription}>
									<h2>Description &nbsp; <a id="alignRight" className={self.state.adminCheck} onClick={self.showEditDescription}>Edit</a></h2>
									<p className="description">{this.props.eventDetails[0].description}</p>
								</div>
								
								<div className={this.state.editDescription}>
									<h2>Edit Description...</h2>
									<textarea className="descriptionTextarea" placeholder={this.props.eventDetails[0].description}></textarea>
									<h2><a className="confirm" onClick={self.setUpdatedDescription}>Confirm</a> &nbsp; <a className="cancel" onClick={self.hideEditDescription}>Cancel</a></h2>
								</div>
							
							</div>

							<div className="aboutHost">

								{/* About the host och Cancel Event */}
								<div className="about">
									<h2>Host: {this.props.eventDetails[0].hostname}</h2>
									<h2 className={this.state.adminCheck}><a className={this.state.cancelButton} id="cancelEvent" onClick={self.showCancelEvent}>Delete Event</a></h2>
									<h2 className={this.state.confirmCancelEvent}><Link to={'/'} className="confirm" onClick={() => this.cancelEventAction()}>Confirm</Link> <a className="cancel" onClick={self.hideCancelEvent}>Cancel</a></h2>
								</div>

							</div>

						</div>


						<div className="eventDetailsRight">
							
							<div className="eventLocation">
								<GoogleMap lng={this.getDefaultLongitude()} lat={this.getDefaultLatitude()} marker={this.getMarkers()} settle={this.props.eventDetails[0].locationSettle} />
								{this.setLocationHeaders()} {this.props.eventDetails[0].location.map(this.locationSet)} {this.props.eventDetails[0].location.map(this.locationVote)}
							</div>

							<div className={this.state.editPlace}>
								
								<Geosuggest className="addPlace" 
									onSuggestSelect={this.fromGeoSuggest}
								/>

							</div>

							<div className="eventDate">
								{this.setDateHeaders()} {this.props.eventDetails[0].start_time.map(this.timeSet)} {this.props.eventDetails[0].start_time.map(this.timeVote)}
							</div>

							<div className={this.state.editDate}>
								<div className="addDateTimePicker">
									<Datetime 
										className = "inputStart" // För att komma åt och styla add Date i eventdetails
										input = {true}
										locale = {"sv"}
										defaultValue = {moment().startOf('hour').add(1, "hours")}
										renderDay = {this.renderDay}
										renderMonth = {this.renderMonth}
										renderYear = {this.renderYear}
										onBlur = {this.addStartDate}
									/>
		
									<Datetime
										className = "inputEnd" // För att komma åt och styla add Date i eventdetails
										locale = {"sv"}
										defaultValue = {moment().startOf('hour').add(2, "hours")}
										renderDay = {this.renderDay}
										renderMonth = {this.renderMonth}
										renderYear = {this.renderYear}
										onBlur = {this.addEndDate}
									/>
								</div>
							
							<div className="addDateTime">
								<h2><a className="confirm" onClick={() => this.fromDatePicker()}>Add Date/Time</a></h2>
							</div>

							</div>

						</div>

					</div>

					</div>
				
				</div>
			)
		}
		
		else {
			return(
				<div>
					Loading...
				</div>
			)
		}
	}
}

function mapStateToProps(state){
	return { eventDetails: state.eventDetails };
}

function mapDispatchToProps(dispatch){
	return bindActionCreators ({ fetchEventDetails, fetchEventAttendees, fetchEvent }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps) (EventDetails);