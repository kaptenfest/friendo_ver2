import React, { Component } from "react";

export class ClosePlaces extends Component {
	
	constructor(){
		super()
		this.handleChange = this.handleChange.bind(this);
	}

	// Skickar "aktivt" voteId till setPlaceState
	handleChange(){
		this.props.closeTheLocation(this.props.placeID) 
	}


	render(){

		self=this

		return(
		
			<div className="placesVoteHolder">
				
				<div className="placesVoter">
					<label htmlFor="voteForPlace"></label>
						<label className="placesRadio">
    				<input type="radio" name="voteForPlace" value={this.props.placeID} onChange={this.handleChange} />
   					<span></span>
					</label>
				</div>

				<div className="placesVotes">
					<p>{this.props.placeCount}</p>
				</div>

			</div>

		)
	
	}
}


ClosePlaces.propTypes = {
	closeTheLocation: React.PropTypes.func,
};
