import React, { Component } from "react";
import { connect } from "react-redux";
// import { bindActionCreators } from "redux";

// Importerar action för att posta "attending" till API
import { chooseEvent } from "../actions/index";


// -------------------------------------------------------------------------

export class EventChoice extends Component {

	// Hanterar valet för att välja om man kommer eller inte
	handleChange(){
		var getSelected = document.getElementById('selecter' + this.props.eventID);
		var selectedValue = getSelected.value;
		var eventID = this.props.eventID;
			chooseEvent(selectedValue, eventID, this.props.token); // Skickas till Actions

		// Uppdatera eventOverview State
		setTimeout(() => {
			this.updateButton(); // Sätter en Timeout för att uppdatera overview när action är utförd.
		}, 500);

	}

	// Uppdaterar Event Overview när valet gjorts (i Event Overview)
	updateButton(){
		self.props.updateOverviewProp();
	}


	render(){

		self=this

		return(
			<div className="attendDropdown">
				<select value = {this.props.eventStatus} className= {this.props.eventStatus} id={'selecter' + this.props.eventID} onChange={this.handleChange.bind(this)}>
					<option value="no_join" disabled hidden>Not Answered</option>
					<option value="join">Attending</option>
					<option value="un_join">Not Attending</option>
					<option value="can_join">Maybe Attending</option>
				</select>
			</div>
		);
	}
}


	EventChoice.propTypes = {
  updateOverviewProp: React.PropTypes.func,
	};

function mapStateToProps(state){
	return { event: state.event };
}

export default connect(null, mapStateToProps) (EventChoice);
