import React, { Component } from "react";

export class CloseDate extends Component {
	
	constructor(){
		super()
		this.handleChange = this.handleChange.bind(this);
	}

	// Skickar "aktivt" voteId till setDateState
	handleChange(){
		this.props.closeTheDate(this.props.dateID) 
	}


	render(){

		self=this

		return(
		
			<div className="datesVoteHolder">
				
				<div className="datesVoter">
					<label htmlFor="voteForDate"></label>
						<label className="datesRadio">
    				<input type="radio" name="voteForDate" value={this.props.dateID} onChange={this.handleChange} />
   					<span></span>
					</label>
				</div>

				<div className="datesVotes">
					<p>{this.props.dateCount}</p>
				</div>

			</div>

		)
	
	}
}


CloseDate.propTypes = {
	closeTheDate: React.PropTypes.func,
};
