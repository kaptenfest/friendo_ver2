import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

// Importerar action för att hämta eventAttendees från API
import { fetchEventAttendees } from "../actions/index.js";

// -------------------------------------------------------

class EventAttendees extends Component {

	// Hämtar attendees från eventDetails
	componentDidMount() {
		this.props.fetchEventAttendees(this.props.realEvent);
	}

	// Plockar ut förnamn
	firstName(name) {
	var fname = name;
	fname = fname.substring(0, fname.indexOf(" "));
		return (
			<strong>{fname}</strong>
		)
	}

	// Returnerar avatar för alla som kommer
	comingAttendeesAvatar(invitedAvatar) {
		return invitedAvatar.status === "join"
	}

	// Returnerar de första 3 attendees 
	joinersAvatar(jointAvatar){
		return <img key={jointAvatar.avatar} className="avatar" src={"http://app.friendoapp.com/" + jointAvatar.avatar}></img>
	}

	// Returnerar namn för alla som kommer
	comingAttendeesName(invitedName){
		return invitedName.status === "join"
	}

	// Returnerar hela namnet för alla som kommer
	joinersName(jointName){
		return (
		<div key={jointName.name}>
			<p className="avatarName">{self.firstName(jointName.name)}</p>
		</div>
		)
	}

	// För att få ut hur många fler än de 3 som får namnet utskrivet
	joinersNameOthers(theRest){
		// console.log("Resterande av attendees: ", theRest)
	}

	// Returnerar de som kommer
	comingList(comingList){
		if(comingList.status === "join"){
			return (
				<div className="weAreComing" key={comingList.name}>
					<img className="avatar" src={"http://app.friendoapp.com/" + comingList.avatar}></img> <p>{comingList.name}</p>
				</div>
			)
		}
	}

	// Returnerar de som kanske kommer 
	maybeList(maybeList){
		if(maybeList.status === "can_join"){
			return (
				<div className="weAreMaybe" key={maybeList.name}>
					<img className="avatar" src={"http://app.friendoapp.com/" + maybeList.avatar}></img> <p>{maybeList.name}</p>
				</div>
			)
		}
	}

	// Returnerar de som inte kommer
	notList(notList){
		if(notList.status === "un_join"){
			return (
				<div className="weAreNot" key={notList.name}>
					<img className="avatar" src={"http://app.friendoapp.com/" + notList.avatar}></img> <p>{notList.name}</p>
				</div>
			)
		}
	}


	render(){

		self=this

		if(this.props.eventAttendees[0]) {
			
			return (
				<div className="attendeesInfo">
					<div className="attendeesAvatar">
						{this.props.eventAttendees.filter(this.comingAttendeesAvatar).slice(0,3).map(this.joinersAvatar)}
					</div>
					<div className="attendeesName">
						{this.props.eventAttendees.filter(this.comingAttendeesName).map(this.joinersName).slice(0,3)}
						{this.props.eventAttendees.filter(this.comingAttendeesName).map(this.joinersNameOthers).length -3 < 0 ? "no more are coming." : this.props.eventAttendees.filter(this.comingAttendeesName).map(this.joinersNameOthers).length -3 + " more are coming."}
					</div>
					
						<div id="comingAttendees" className={this.props.coming}>
						<h1 className="comingHeader"><strong>Attending</strong></h1>
							{this.props.eventAttendees.map(this.comingList)}
						<h1 className="closeHeader"><strong><button onClick={() => this.props.closeList()} className="closeHeaderButton">Close</button></strong></h1>
						</div>
						<div id="maybeAttendees" className={this.props.maybe}>
						<h1 className="maybeHeader"><strong>Maybe</strong></h1>
							{this.props.eventAttendees.map(this.maybeList)}
						<h1 className="closeHeader"><strong><button onClick={() => this.props.closeList()} className="closeHeaderButton">Close</button></strong></h1>
						</div>
						<div id="notAttendees" className={this.props.not}>
						<h1 className="notHeader"><strong>Not Attending</strong></h1>
							{this.props.eventAttendees.map(this.notList)}
						<h1 className="closeHeader"><strong><button onClick={() => this.props.closeList()} className="closeHeaderButton">Close</button></strong></h1>
						</div>
				</div>
			)
		}
		
		else {
			return(
				<div>
					Loading...
				</div>
			)
		}
	}
}

EventAttendees.propTypes = {
  closeList: React.PropTypes.func,
};

function mapStateToProps(state){
	return { eventAttendees: state.eventAttendees };
}

function mapDispatchToProps(dispatch){
	return bindActionCreators ({ fetchEventAttendees }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps) (EventAttendees);