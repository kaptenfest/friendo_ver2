import React, { Component } from "react";

export class EditDate extends Component {
	
	constructor(){
		super()
		this.handleChange = this.handleChange.bind(this);
	}

	handleChange(){
		this.props.setTheDate(this.props.date) 
	}


	render(){

		self=this

		return(
		
			<div className="datesVoteHolder">
				
				<div className="datesVoter">
					<img className="del" src="../img/eventDetails/del.png" onClick={() => this.handleChange(this.props.date)}></img>
				</div>

				<div className="datesVotes">
					<p>{this.props.dateCount}</p>
				</div>

			</div>

		)
	
	}
}


EditDate.propTypes = {
	setTheDate: React.PropTypes.func,
};
