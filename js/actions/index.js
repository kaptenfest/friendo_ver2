import axios from "axios";
const querystring = require('querystring');

import { EventChoice } from "../containers/eventChoice";

import { EventDetails } from "../containers/eventDetails";

import { DatesChoice } from "../containers/datesChoice";

import { Login } from "../../login/login";


// Hämtar alla event
export function fetchEvent(user_id) {
	const request = axios({
  	method: "GET",
  	headers: {'Content-Type': 'application/x-www-form-urlencoded'},
  	url: "/login/events-web/" + user_id,
  	baseURL: "http://app.friendoapp.com/api"
	})

	return {
		type: "FETCH_EVENT",
		payload: request
	};
}

// Sorterar alla events
export function sortEvents(sorted){

  return {
    type: "SORT_EVENT",
    payload: sorted
  }

}

// Byter status
export function chooseEvent(value, eventID, token) {
	const submit = axios({
		method: "POST",
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		url: "/event/comfirminvite",
		baseURL: "http://app.friendoapp.com/api",

		data: querystring.stringify({
  	  token: token,
  	  id: eventID,
  	  choose: value
  	}),
	})

};

// Hämtar Event Details
export function fetchEventDetails(id, token) { // Skicka med eventID
  const request = axios({
    method: "POST",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    url: "/event/details",
    baseURL: "http://app.friendoapp.com/api",

    data: querystring.stringify({
      token: token,
      id: id
    }),

  })

    return {
      type:"FETCH_EVENT_DETAILS",
      payload: request
    }

};

// Hämtar event attendees
export function fetchEventAttendees(theEvent) {
  const request = axios ({
    method: "POST",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    url: "/event/list-user-of-event-by-any-status/",
    baseURL: "http://app.friendoapp.com/api",

    data: querystring.stringify({
      eventId: theEvent
    }),
  })

    return {
      type: "FETCH_EVENT_ATTENDEES",
      payload: request
    }
}

// Hämtar info om den inloggade användaren (För profilsidan i framtiden)
export function fetchUserInfo(token) {
  const request = axios ({
    method: "POST",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    url: "/user/getuserinfo/",
    baseURL: "http://app.friendoapp.com/api",

    data: querystring.stringify({
      token: token
    }),
  })

    return {
      type: "FETCH_USER_INFO",
      payload: request
    }
}

// Rösta på plats för event
export function choosePlace(thisPlace, token) {
  const submit = axios({
    method: "POST",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    url: "/event/voteresult/",
    baseURL: "http://app.friendoapp.com/api",

    data: querystring.stringify({
      token: token,
      type_choice: "location",
      voteid: thisPlace
      
    }),
  })

};

// Rösta på tid/datum för event
export function chooseDate(thisDate, token) {
    const submit = axios({
    method: "POST",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    url: "/event/voteresult/",
    baseURL: "http://app.friendoapp.com/api",

    data: querystring.stringify({
      token: token,
      type_choice: "start_time",
      voteid: thisDate
      
    }),
  })

    return {
      type:"CHOOSE_DATE",
      payload: submit 
    }

};

// Avsluta röstningen för en plats
export function closeLocationAction(voteid, eventId, token){
  const submit = axios ({
    method: "POST",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    url: "/event/approve-vote-event",
    baseURL: "http://app.friendoapp.com/api",

    data: querystring.stringify({
      token: token,
      typeChoise: "location",
      voteId: voteid,
      eventId: eventId
    }),
  })

}

// Avsluta röstningen för tid/datum
export function closeDateAction(voteid, eventId, token){
  const submit = axios ({
    method: "POST",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    url: "/event/approve-vote-event",
    baseURL: "http://app.friendoapp.com/api",

    data: querystring.stringify({
      token: token,
      typeChoise: "start_time",
      voteId: voteid,
      eventId: eventId
    }),
  })

}

// Ta bort en plats
export function delPlaceAction(name, start, place, imageFromServer, description, limitperson, id, token){
  const submit = axios ({
    method: "POST",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    url: "/event/editevent",
    baseURL: "http://app.friendoapp.com/api",

    data: querystring.stringify({
      token: token,
      name: name,
      start_time: "[" + start + "]",
      end_time: "",
      location: "[" + place + "]",
      imageFromServer: imageFromServer,
      image: "",
      description: description,
      limitperson:limitperson,
      id: id
    })
  })

}

// Lägg till en plats
export function addPlaceAction(name, start, place, imageFromServer, description, limitperson, id, token){
  const submit = axios ({
    method: "POST",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    url: "/event/editevent",
    baseURL: "http://app.friendoapp.com/api",

    data: querystring.stringify({
      token: token,
      name: name,
      start_time: "[" + start + "]",
      end_time: "",
      location: "[" + place + "]",
      imageFromServer: imageFromServer,
      image: "",
      description: description,
      limitperson:limitperson,
      id: id
    })
  })

}

// Ta bort en tid/datum
export function delDateAction(name, start, place, imageFromServer, description, limitperson, id, token){
  const submit = axios ({
    method: "POST",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    url: "/event/editevent",
    baseURL: "http://app.friendoapp.com/api",

    data: querystring.stringify({
      token: token,
      name: name,
      start_time: "[" + start + "]",
      end_time: "",
      location: "[" + place + "]",
      imageFromServer: imageFromServer,
      image: "",
      description: description,
      limitperson:limitperson,
      id: id
    })
  })

}

// Lägg till en tid/datum
export function addDateAction(name, start, place, imageFromServer, description, limitperson, id, token){
  const submit = axios ({
    method: "POST",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    url: "/event/editevent",
    baseURL: "http://app.friendoapp.com/api",

    data: querystring.stringify({
      token: token,
      name: name,
      start_time: "[" + start + "]",
      end_time: "",
      location: "[" + place + "]",
      imageFromServer: imageFromServer,
      image: "",
      description: description,
      limitperson:limitperson,
      id: id
    })
  })

}

// Editera titel på eventet
export function changeTitleAction(name, start, place, imageFromServer, description, limitperson, id, token){
  const submit = axios ({
    method: "POST",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    url: "/event/editevent",
    baseURL: "http://app.friendoapp.com/api",

    data: querystring.stringify({
      token: token,
      name: name,
      start_time: "[" + start + "]",
      end_time: "",
      location: "[" + place + "]",
      imageFromServer: imageFromServer,
      image: "",
      description: description,
      limitperson:limitperson,
      id: id
    })
  })

}

// Editera beskrivning av eventet
export function changeDescriptionAction(name, start, place, imageFromServer, description, limitperson, id, token){
  const submit = axios ({
    method: "POST",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    url: "/event/editevent",
    baseURL: "http://app.friendoapp.com/api",

    data: querystring.stringify({
      token: token,
      name: name,
      start_time: "[" + start + "]",
      end_time: "",
      location: "[" + place + "]",
      imageFromServer: imageFromServer,
      image: "",
      description: description,
      limitperson:limitperson,
      id: id
    })
  })

}

// Hämta datum där det är ett event
export function fetchEventDates(token){
  const request = axios ({
    method: "POST",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    url: "/user/user-event-calendar",
    baseURL: "http://app.friendoapp.com/api",

    data: querystring.stringify({
      token: token
    }),
  })

    return {
      type: "FETCH_EVENT_DATE",
      payload: request
    }

}

// Ta bort ett event (admin)
export function cancelEvent(eventId, userId){
  const request = axios ({
    method: "POST",
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    url: "/event/delete-event",
    baseURL: "http://app.friendoapp.com/api",

    data: querystring.stringify({
      eventId: eventId,
      userId: userId
    }),
  })
}