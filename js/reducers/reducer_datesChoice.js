import { chooseDate } from "../actions/index.js";

export default function (state = [], action) {
	switch (action.type) {
	case "CHOOSE_DATE":
		return state.concat(action.payload.data); 
	}
	return state;
}