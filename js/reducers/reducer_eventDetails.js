import { fetchEventDetails } from "../actions/index.js";

export default function (state = [], action) {
	switch (action.type) {
	case "FETCH_EVENT_DETAILS":
		state.splice(0, state.length)
		return state.concat(action.payload.data.events);
	}
		return state;
}