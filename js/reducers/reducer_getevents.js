import { fetchEvent } from "../actions/index.js";
import { sortEvents } from "../actions/index.js";

export default function fetch (state = [], action) {
	switch (action.type) {
	case "FETCH_EVENT":
		state.splice(0, state.length)
		return state.concat(action.payload.data.events[0].ievent);
	case "SORT_EVENT":
		state.splice(0, state.length)
		return state.concat(action.payload);
	}
		
		return state;
}

