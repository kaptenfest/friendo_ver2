import { filterEvents } from "../actions/index.js";

export default function filter (state = [], action) {
	switch (action.type) {
	case "FILTER_EVENTS":
		return state.concat(action.payload.filter);
	}
	return state;
}