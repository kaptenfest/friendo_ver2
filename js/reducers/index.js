import { combineReducers } from 'redux';
import EventReducer from "./reducer_getevents";
import EventDetails from "./reducer_eventDetails";
import EventAttendees from "./reducer_eventAttendees";
import UserInfo from "./reducer_userInfo";
import ChooseDate from "./reducer_datesChoice";
import FetchEventDate from "./reducer_showEventDate";

const rootReducer = combineReducers({
  event: EventReducer,
  eventDetails: EventDetails,
  eventAttendees: EventAttendees,
  userInfo: UserInfo,
  chooseDate: ChooseDate,
  eventDate: FetchEventDate
});

export default rootReducer;
