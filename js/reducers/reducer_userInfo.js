import { fetchUserInfo } from "../actions/index.js";

export default function fetchInfo (state = [], action) {
	switch (action.type) {
	case "FETCH_USER_INFO":
		return state.concat(action.payload.data);
	}
		return state;
}