import { fetchEventAttendees } from "../actions/index.js";

export default function fetchAttendees (state = [], action) {
	switch (action.type) {
	case "FETCH_EVENT_ATTENDEES":
		state.splice(0, state.length)
		return state.concat(action.payload.data.listUserVote);
	}
		return state;
}