/*
  *React date-time-picker is used for selecting date and time, 
   documentation is availible at https://github.com/YouCanBookMe/react-datetime 
*/

var React = require('react');
var moment = require('moment');
var Datetime = require('react-datetime');
import DateTimeCard from "./date_modules/date_time_card";

var Eventdate = React.createClass({
    getInitialState: function() {
      return {
        card: [],
        startTime: "",
        endtime: "",
        lang: "sv"
      };
    },
    componentDidMount() {
        var datePicker = document.getElementsByClassName("rdt");
        document.getElementsByClassName("rdt")[0].style.display = "none";
        document.getElementsByClassName("rdt")[1].style.display = "none";
        for (var i = 0; i < datePicker.length; i++) {
         datePicker[i].style.display = "none";
        }
        //Fetch time_card from sessionStorage and create an array of time cards containing the values from sessionStorage
        //Set this.state.card to be equal to the array
        var card = this.state.card;
        if(sessionStorage.time_card){
          var storage = JSON.parse(sessionStorage.getItem("time_card"));
                         
         for (var i = 0; i < storage.length; i++){
            if(storage[i] !== ""){
              card.push(<DateTimeCard 
                startTime={storage[i].props.startTime}
                endTime={storage[i].props.endTime}
                key={i}
                btnId={i}
                click={this.removeCard}
              />)
            }
            else {
              card.push("")
            }
          }
          this.setState({card: card});
       }
    },
    //Remove card from UI and sessionStorage

    removeCard(e) {
      //Items are replaced with empty string to prevent unwanted behavior
       var cardArr = this.state.card;
           cardArr.splice(e.target.id, 1,"");
       var startTimeArr = JSON.parse(sessionStorage.getItem("startTime"));
           startTimeArr.splice(e.target.id,1,"");
       var endTimeArr = JSON.parse(sessionStorage.getItem("endTime"));
           endTimeArr.splice(e.target.id, 1,"");

        this.setState({card: cardArr});
        sessionStorage.setItem("time_card",JSON.stringify(cardArr));
        sessionStorage.setItem("startTime",JSON.stringify(startTimeArr));
        sessionStorage.setItem("endTime",JSON.stringify(endTimeArr));
    },
    //Opens date/time picker when add new time button is pressed
    toggleInput() {
      var datePicker = document.getElementsByClassName("rdt");
      var input = document.getElementsByClassName("form-control");
       for (var i = 0; i < datePicker.length; i++) {
       datePicker[i].style.display = "inline-block";
       input[i].value = "";
      }
      document.getElementById("btn-add-location").style.display = "none";
      document.getElementById("btn-clear").style.display = "inline-block";
      document.getElementById("btn-add").style.display = "inline-block";
    },
    //Clear date/time input field when button is clicked
    clearTime() {
      var input = document.getElementsByClassName("form-control");
       for (var i = 0; i < input.length; i++) {
        input[i].value = "";
      }
    },
    //Convertes value(s) from start/ end time input fields to unix time
    getUnixTime() {
      var startTime = document.getElementsByClassName('form-control')[0].value;
      var endTime = document.getElementsByClassName('form-control')[1].value;
      var date = new Date();
      var currentTime = date.getTime();
      var startEndTimeCheck = true;
      var startTimeArr = JSON.parse(sessionStorage.getItem("startTime"));
      var endTimeArr = JSON.parse(sessionStorage.getItem("endTime"));
      
      Date.prototype.getUnixTime = function() { return this.getTime() };
      if(!Date.now) Date.now = function() { return new Date(); }
      Date.time = function() { return Date.now().getUnixTime(); }
      
      //Parse a date and get it as Unix time
      startTime = new Date(startTime).getUnixTime();
      endTime = new Date(endTime).getUnixTime();

        //Check if same start/endtimes have already been added
        for(var i = 0; i < startTimeArr.length; i++) {
          if(startTimeArr[i] === startTime && endTimeArr[i] === endTime) {
            alert("Oops! You can't add the same start and end-time twice")
            startEndTimeCheck = false;
            break;
          }
        }
        //If start/end times have not already been added
        if(startEndTimeCheck) {
          if(startTime < currentTime || endTime < currentTime ){
          alert("Start time can't be earlier than current time");
          }
          else if(endTime <= startTime) {
            alert("End time must be later than start time")
          }
          else{
            if(sessionStorage.startTime) {
              let unixStartTime = sessionStorage.getItem("startTime");
                  unixStartTime = JSON.parse(unixStartTime);
                  unixStartTime.push(startTime);
              sessionStorage.setItem("startTime",JSON.stringify(unixStartTime));
            }
            else {
              let unixStartTime = [];
                  unixStartTime.push(startTime);
                  sessionStorage.setItem("startTime",JSON.stringify(unixStartTime));
            }
            if(sessionStorage.endTime) {
              let unixEndTime = sessionStorage.getItem("endTime");
                  unixEndTime = JSON.parse(unixEndTime);
                  unixEndTime.push(endTime);
              sessionStorage.setItem("endTime",JSON.stringify(unixEndTime));
            }
            else {
              let unixEndTime = [];
              unixEndTime.push(endTime);
              sessionStorage.setItem("endTime",JSON.stringify(unixEndTime));
            }
          
          this.getDate(startTime, endTime);
          }
        }
     },
    //Converts unix start/end times to human readable format.
    getDate(startTime, endTime) {
      var days = ['sön','mån', 'tis', 'ons','tors','fre','lör'];
      var months = ['jan','feb','mars','apr','maj','jun','jul','aug','sept','okt','nov','dec'];
      var startDay = new Date(startTime).getDay();
          startDay = days[startDay];
      var endDay = new Date(endTime).getDay();
          endDay = days[endDay];
      var startDate = new Date(startTime).getDate();
      var endDate = new Date(endTime).getDate(); 
      var startMonth = new Date(startTime).getMonth();
          startMonth = months[startMonth];
      var endMonth = new Date(endTime).getMonth();
          endMonth = months[endMonth];
      var startHour = new Date(startTime).getHours();
      var endHour = new Date(endTime).getHours();
      var startMinute = new Date(startTime).getMinutes();
      var endMinute = new Date(endTime).getMinutes();
      
      //Strings to be displayed in date/time cards
      startTime = startDay + " " + startDate + " " + startMonth + " " + testTime(startHour) + ":" + testTime(startMinute); 
      endTime = endDay + " " + endDate + " " + endMonth + " " + testTime(endHour) + ":" + testTime(endMinute);
      
      //Prevents end time to be displayed if not selected
      var time = {
                  start_time: startTime, 
                  end_time: endTime === "undefined NaN undefined NaN:NaN" ? "" : endTime
                 };
      var key;
      var cardArr = this.state.card;
          cardArr[0] === undefined ? key = 0 : key = cardArr.length
      //Cards to be displayed when user adds date and time
      var card = (<DateTimeCard 
                  startTime={time.start_time}
                  endTime={time.end_time}
                  key={key}
                  btnId={key}
                  click={this.removeCard}
                  />)
      cardArr.push(card)
      this.setState({card: cardArr})//Sets cardArr in state. Added date/time will be visible
      cardArr = JSON.stringify(cardArr)
      sessionStorage.setItem("time_card", cardArr)
      //Adds a zero before hours and minutes if < 10
      function testTime(time){
        if(time < 10){
          time = '0' + time;
        }
        return time;
      }
      document.getElementsByClassName("rdt")[0].style.display = "none";
      document.getElementsByClassName("rdt")[1].style.display = "none";
      document.getElementById("btn-add-location").style.display = "block";
      document.getElementById("btn-clear").style.display = "none";
      document.getElementById("btn-add").style.display = "none";
    },
    render: function(){
      let datePickerStyle = {
        width: 220
      }

     return( 
      <div id="date-container">
        <div id="date-time-card-container">
          <ul className="card-container" id="date-time-card-list">
            {this.state.card}
          </ul>
        </div>
        <div id="date-time-input">
          <Datetime 
            style={this.datePickerStyle}
            locale={this.state.lang}
            inputProps={{placeholder: 'start time'}}
            timeConstraints={{minutes:{step:15}}}
            renderDay={ this.renderDay }
            renderMonth={ this.renderMonth }
            renderYear={ this.renderYear }
            defaultValue={moment().startOf('hour').add(1,'hours')}     
                          
          />
          <Datetime 
            locale={this.state.lang}
            inputProps={{placeholder: 'end time (optional)'}}
            timeConstraints={{minutes:{step:15}}}
            renderDay={ this.renderDay }
            renderMonth={ this.renderMonth }
            renderYear={ this.renderYear }
            defaultValue={moment().startOf('hour').add(2,'hours')} 
          />
        </div>
        <div>
          <button id="btn-add-location" onClick={this.toggleInput} type="submit">ADD NEW TIME</button>
          <button id="btn-clear" onClick={this.clearTime}>Clear Time</button>
          <button id="btn-add" onClick={this.getUnixTime}>Add Time</button>
        </div>
      </div>);
    },
    renderDay: function( props, currentDate, selectedDate ){
        return <td {...props}>{ currentDate.date() }</td>;
    },
    renderMonth: function( props, month, year, selectedDate){
        return <td {...props}>{ month }</td>;
    },
    renderYear: function( props, year, selectedDate ){
        return <td {...props}>{ year % 100 }</td>;
    }
});
module.exports = Eventdate;

