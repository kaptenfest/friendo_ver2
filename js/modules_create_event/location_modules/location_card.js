import React from "react";
export default class LocationCard extends React.Component {
	render() {
		return(
			<li className="card" id={this.props.id}>
			
				<img id="location-icon" src="./img/location_icon.png" alt=""/>
				
				<span className="card-span-bold"id="location-name">{this.props.name}</span><br />
				<span id="location-address">{this.props.address}</span>
				
				<button onClick={this.props.click} id="remove-location">
				<img id={this.props.btnId} src="./img/ic_cross.png" alt=""/>
				</button>
			</li>
		);
	}
}
