


  export var placeSearch, autocomplete;
      
  export function initAutocomplete() {
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    autocomplete = new google.maps.places.Autocomplete(
        (document.getElementById('autocomplete')),
        {types: ['geocode','establishment']});

    // When the user selects an address from the dropdown
   
    autocomplete.addListener('place_changed', getPlace);
  }

  export function getPlace() {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();
		console.log(place.name+'\n' + place.vicinity);
		console.log("Lat: "+(place.geometry.viewport.f.b + place.geometry.viewport.f.f) / 2 );
		console.log("Long: " +(place.geometry.viewport.b.b + place.geometry.viewport.b.f) / 2 );
		document.getElementById("autocomplete").value="";
		
	}

  // Bias the autocomplete object to the user's geographical location,
  // as supplied by the browser's 'navigator.geolocation' object.
  export function geolocate() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        var geolocation = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        var circle = new google.maps.Circle({
          center: geolocation,
          radius: position.coords.accuracy
        });
        autocomplete.setBounds(circle.getBounds());
      });
    }
}
