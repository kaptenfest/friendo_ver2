/*
 *documentation for google places api is available at https://developers.google.com/places/documentation/
*/

import React from "react";
import LocationCard from "./location_modules/location_card";
import Geosuggest from 'react-geosuggest';

export default class Eventlocation extends React.Component{
constructor() {
	super();
	this.state = {
		card: []
	};
}
/*Show  location input field*/
showInput() {
var input = document.getElementsByClassName("location-input")[0];
 	input.style.display = "inline-block";
}
componentDidMount() {
	//Fetch data from sessionStorage and display location cards
	var card = this.state.card;
	if(sessionStorage.address) {
		var storage = JSON.parse(sessionStorage.getItem("address"));
		
	for (var i = 0; i < storage.length; i++) {
		if(storage[i] !== ""){
			card.push(<LocationCard 
			  	name={storage[i].props.name}
			  	address={storage[i].props.address}
			  	key={i}
			  	id={i}
			  	click={this.removeLocation.bind(this)}
		      	btnId={i}
		  	/>)
		}
		else {
			card.push("")
		}
	}
	this.setState({card: card});
	}
}
//Remove location card from UI and sessionStorage
removeLocation(e) {
	//Items are replaced with empty string to prevent unwanted behavior
	var locationCardArr = this.state.card;
			locationCardArr.splice(e.target.id, 1, "")
	var lngArr = JSON.parse(sessionStorage.getItem("lng"));
			lngArr.splice(e.target.id,1,"");
	var latArr = JSON.parse(sessionStorage.getItem("lat"));
			latArr.splice(e.target.id,1,"");
	
	//Re-render location cards and set items in sessionStorage 
	this.setState({card: locationCardArr});
	sessionStorage.setItem("address", JSON.stringify(locationCardArr));
	sessionStorage.setItem("lat", JSON.stringify(latArr));
	sessionStorage.setItem("lng", JSON.stringify(lngArr));
}
//When item address or place is selected from dropdown
onSuggestSelect(suggest){
    var lat = [];
	var lng = [];
	var address = [];
	var addrString = suggest.gmaps.formatted_address;
	 	addrString = addrString.split(',',3);
	var addrObj;
	var checkLocation = true;

	/*Prevents user from adding the same location twice*/
	if(sessionStorage.lat && sessionStorage.lng) {
		lat = JSON.parse(sessionStorage.getItem("lat"));
		lng = JSON.parse(sessionStorage.getItem("lng"));
	}
	for (var i = 0; i < lat.length; i++) {
		if(suggest.location.lat === lat[i] && suggest.location.lng === lng[i]) {
			alert("Oops! This location has already been added");
			checkLocation = false;
			break;
		}
	}
	/*If location hasn't already been added*/
	if(checkLocation) {
		lat.push(suggest.location.lat)
		lng.push(suggest.location.lng)
		
	  	if (addrString[2] === undefined) {
	  		addrObj = {
	  		name: addrString[0].trim(),
	  		address: addrString[1].trim()
	  		};
	  	}
	  	else{
	  		addrObj = {
	  		name: addrString[0].trim(),
	  		address: addrString[1].trim() + "," + addrString[2].trim()
	  		};
	  	}

	  	var key;
	    var cardArr = this.state.card
	        cardArr[0] === undefined ? key = 0 : key = cardArr.length;
	        
	    var card = (<LocationCard
	                  name={addrObj.name}
	                  address={addrObj.address}
	                  key={key}
	                  btnId={key}
	                  click={this.removeLocation.bind(this)}
	                />)
	    cardArr.push(card)
	   
		sessionStorage.setItem("address", JSON.stringify(cardArr))
		sessionStorage.setItem("lat", JSON.stringify(lat));
	  	sessionStorage.setItem("lng", JSON.stringify(lng));

	    this.setState({card: cardArr});
	    
	    document.getElementById("location-field").className="location-field-hidden";
		}
    }
	toggleInput(){
		var target = document.getElementById("location-field");
		document.getElementsByClassName("geosuggest__input")[0].value = "";
		if(target.className === "location-field-hidden") {
			target.className = "location-field-visible"
		}
		else{
			target.className = "location-field-hidden"
		}
	}
render() {
 	self = this;
	  return (
    	<div id="location-container">
      	<div id="location-card-container">
          <ul className="card-container" id="places-card-container">
            {this.state.card}
          </ul>
        </div>
         <div id="location-field" className="location-field-hidden">
  	      <Geosuggest 
            types={['establishment', 'geocode']}
  	      	onSuggestSelect={this.onSuggestSelect.bind(this)}
  	      	onFocus= {(e) => {
  	      					setTimeout(() => { 
  	      					document.getElementsByClassName("geosuggest__input")[0].value=""
  	      					}, 100);
  	      				}}
          />
	     </div>
        <div>
          <button onClick={this.toggleInput.bind(this)} id="btn-add-location" type="submit">ADD NEW LOCATION</button>
        </div>
    </div>
    );
  }
}