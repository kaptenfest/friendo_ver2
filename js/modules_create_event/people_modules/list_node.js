import React from "react";

export default class ListNode extends React.Component {
	render() {
		return(
			<li id={this.props.listNodeId} 
				onClick={this.props.click} >{this.props.content}
			
			</li>
		);
	}
}