import React from "react";

export default class PeopleCard extends React.Component{
	render(){
		return(
			<li className="people-card" id={this.props.id}>
				<span className="card-span-bold" 
					  id="location-name">
					  {this.props.name}
				</span>
				<button onClick={this.props.removeCard}>
				<img src="./img/ic_cross.png" 
					 id={this.props.btnId} 
					 alt="remove contact"/>
				</button>
			</li>
		);
	}
}