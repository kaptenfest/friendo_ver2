import React from "react";
import axios from "axios";
import PeopleCard from "./people_modules/people_card";
import ListNode from "./people_modules/list_node";

export default class People extends React.Component{
  constructor(props) {
  	super(props);
  	this.state = {
  		inputVal: "",
  		friends: [],
  		friendSearch: [],
  		output: [],
  		card:[]
  	};
  }
  
  componentDidMount() {
  	var card = this.state.card;
  	var friendsArr = JSON.parse(sessionStorage.getItem("friends"));
  	var friendSearch = JSON.parse(sessionStorage.getItem("friends"));
  	this.setState({friends: friendsArr});
		this.setState({friendSearch: friendSearch});
  	
  	if(sessionStorage.people_card) {
  		var storage = JSON.parse(sessionStorage.getItem("people_card"));
  		  
		for (var i = 0; i < storage.length; i++) {
			if(storage[i] !== ""){
				card.push(<PeopleCard 
					removeCard={this.removeCard.bind(this)}
					name={storage[i].props.name}
					key={i}
					btnId={i}
				/>)
			}
			else {
				card.push("")
			}
		}
		this.setState({card: card});
  	}
  }
  //Remove card from UI and sessionStorage when remove button on card is clicked. 
  //Corresponding friend id is also removed from session storage
  removeCard(e) {
  	//Items are replaced with empty string to prevent unwanted behavior
  	var id = e.target.id;
  	var cardArr = this.state.card;
  			cardArr.splice(id,1,"");
  	var friendId = JSON.parse(sessionStorage.getItem("friendId"))
  			friendId.splice(id,1,"")
  	var email = JSON.parse(sessionStorage.getItem("email"))
  			email.splice(id,1,"")
  	
  		
  	this.setState({card: cardArr})
  	sessionStorage.setItem("friendId", JSON.stringify(friendId));
    sessionStorage.setItem("people_card", JSON.stringify(cardArr));
    sessionStorage.setItem("email", JSON.stringify(email));
    
  }
  onInputChange(e) {

  	//If @ is present in value from input
  	let emailPattern = new RegExp("@");
  	let test = emailPattern.test(e.target.value);

  	//If test evaluates to true add email button will appear
  	if (test) {
		let targetClassName = document.getElementsByClassName("add-email")[0];
		document.getElementById("search-user").className = "email";
  		document.getElementById("btn-invite-email").style.display = "block";
  		document.getElementById("btn-invite-email").className = "btn-invite-email-visible";
  		if(targetClassName) {
  			targetClassName.className = "add-email-visible";
  		}
	  }
   	//If test doesn't evaluate to true and className = email add email button will no longer be visible
  	if (!test && e.target.className === "email") {
  		document.getElementById("search-user").className = "search-user";
  		document.getElementById("btn-invite-email").style.display = "none";

  		if(document.getElementsByClassName("add-email-visible")[0]) { //To avoid error message
  			document.getElementsByClassName("add-email-visible")[0].className = "add-email";
  		}
  	}
  	//If target's className is search-user a dropdown list will appear
  	if (e.target.className === "search-user") {

	  	let friendsArr = this.state.friends;
	   	let friendSearch = this.state.friendSearch;
	   	let removeWhitespace = /\s/g;
	  	let results = document.getElementById("output-container");
	  	let value = e.target.value;
					value = value.toLowerCase();
					value = value.replace(removeWhitespace, "");
	  	let length = value.length;
	  	let output = [];
	  	let name;
	  	let check;
			//Converts all letters to lower case and removes all whitespaces
			for (var i = 0; i < friendsArr.length; i++) {
				friendsArr[i].name = friendsArr[i].name.toLowerCase();
				friendsArr[i].name = friendsArr[i].name.replace(removeWhitespace, "");
			}
			//Loops thuru friendsArr
			for (var i = 0; i < friendsArr.length; i++){
				name = friendsArr[i].name;
				check = true;
				//Loops thuru name values and checks characters
				for (var j = 0; j < length; j++) {
					//If characters at pos j not the same
					if(name.charAt(j) !== value.charAt(j)) {
							check = false;
						}
					}
				if(check) {
					output.push(<ListNode 
						click={this.onListItemClick.bind(this)}
						listNodeId={friendSearch[i].id}
						content={friendSearch[i].name}
						key={friendSearch[i].id}
						btnId={i}
						/>
					);
				}
			}
			this.setState({output: output })
			if(value === "") {
				this.setState({output: []});
				results.className = "output-container-hidden";
	  	}
	  	else{
	  		results.className = "output-container-visible";
	  	}
	  }
	}
	//When item is selected from dropdown 
	onListItemClick(e) {
		//Adds border and margin to input field
		document.getElementById("search-user").style.borderTop = "2px solid rgba(0,0,0,0.1)";
		document.getElementById("search-user").style.borderBottom = "2px solid rgba(0,0,0,0.1)";
		document.getElementById("search-user").style.marginTop = "5px";

		let inputVal = document.getElementById("search-user").value;
		let key;
		let friendId;
		let email;
		let cardArr = this.state.card;
			cardArr[0] === undefined ? key = 0 : key = cardArr.length;
		let storage = JSON.parse(sessionStorage.getItem("friends"));
			if(typeof(e) === "string") {
				inputVal = e;
			}

			else if(e.target) {
				if(e.target.id !== "btn-invite-email"){
					inputVal = e.target.innerHTML;
				}
			}
			//When contact is selected by clicking return key
			else {
				inputVal = e.innerHTML;
			}

		var card = <PeopleCard 
					name={inputVal}
					removeCard={this.removeCard.bind(this)}
					key={key}
					btnId={key}
					/>

		sessionStorage.friendId ? friendId = JSON.parse(sessionStorage.getItem("friendId")) : friendId = [];
		sessionStorage.email ? email = JSON.parse(sessionStorage.getItem("email")) : email = [];
		cardArr.push(card)
			if(typeof(e) === "string") {
				friendId.push("");
				email.push(inputVal);
			}
			else if(e.target) {
				if(e.target.id === "btn-invite-email") {//KNAPP!!!!!!!!
					friendId.push("");
					email.push(inputVal);
				}	
				else {
					friendId.push(e.target.id);
					email.push("");
				}
			}
			//When contact is selected by clicking return key
			else {
				friendId.push(e.id);
				email.push("");
			}
		
		//Set items in sessionStorage
		sessionStorage.setItem("email", JSON.stringify(email))
		sessionStorage.setItem("friendId", JSON.stringify(friendId));
		sessionStorage.setItem("people_card", JSON.stringify(cardArr));//Sets cardarr in storage
		this.setState({card: cardArr});//Sets cardArr in state

		var outputContainer = document.getElementsByClassName("output-container-visible")[0];
			outputContainer.className="output-container-hidden";

				document.getElementById("search-user").value = "";
				document.getElementById("btn-invite-email").style.display ="none";

				if(document.getElementsByClassName("add-email-visible")[0]) {
					document.getElementsByClassName("add-email-visible")[0].className = "add-email";
				}
				
	}
	/*Browse dropdown list using arrow keys*/
	browseList(keyCode) {
		let children = document.getElementById("people-search-list").childNodes;
		
		if(keyCode === 40){
			for (let i = 0; i < children.length; i++) {
				if(children[i].className === "people-search-list-item-selected") {
					children[i].classList.remove('people-search-list-item-selected')
					children[i+1] === undefined ? children[0].classList.add('people-search-list-item-selected') : children[i+1].classList.add('people-search-list-item-selected')
					break;
				}
				else {
					children[0].classList.add('people-search-list-item-selected')
					
				}
			}
		}
		if(keyCode === 38){
			
			for (let i = 0; i < children.length; i++) {
				if(children[i].className === "people-search-list-item-selected")  {
					children[i].classList.remove('people-search-list-item-selected')
					children[i-1] === undefined ? children[children.length-1].classList.add('people-search-list-item-selected') : children[i-1].classList.add('people-search-list-item-selected')
					break;
				}
				else {
					children[0].classList.add('people-search-list-item-selected')
					
				}
			}
		}
	}
  render() {
      return (
      	<div>
      		<div id="invited-people-container">
            <ul className="card-container" id="people-card-container">
            	{this.state.card}
            </ul>
          </div>
	        <div id="search-user-container">
	          <input onChange={this.onInputChange.bind(this)} 
          			 id="search-user" 
          			 className="search-user" 
	          		 type="text" 
	          		 placeholder="search user or type e-mail address"
          			 onKeyDown={(event) => {

      			 		let keyCode = event.keyCode;

      			 		if (keyCode === 40 || keyCode === 38) {
      			 			event.preventDefault();
      			 			this.browseList(keyCode);
      			 		}  
      			 		if (keyCode === 13) {

      			 			let emailPattern = new RegExp("@");
  							let test = emailPattern.test(event.target.value);

  							if(!test) {
  							   let target = document.getElementsByClassName('people-search-list-item-selected')[0]
								this.onListItemClick(target)
  							}
  							else {
  								let target = event.target.value;
  								this.onListItemClick(target)
  							}
      			 		}		
          			 }}
	          	/> 
	        </div>
	        <div id="output-container" className="output-container-hidden">
	        	<ul id="people-search-list">
	        	{this.state.output}
	         	</ul>
	        </div>
	        <div className="add-email">
	        	<button 
    				id="btn-invite-email" 
    				onClick={this.onListItemClick.bind(this)}>Click to add email
	        	</button>
	        </div>
	      </div>
      );
    }
}
