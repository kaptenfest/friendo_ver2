import React from "react";

export default class Details extends React.Component{
  //Fetch event name and event details from sessionStorage
  componentDidMount() {
    var eventName = sessionStorage.eventName === "undefined" ? "" : sessionStorage.getItem("eventName")
    var eventDetail = sessionStorage.eventDetail === "undefined" ? "" : sessionStorage.getItem("eventDetail")
    //Set value of input and textarea to values from sessionStorage
    document.getElementById('event-name').value = eventName;
    document.getElementById('event-details').value = eventDetail;
  }
    
  render() {
    return (
      <div>
        <div id="event-name-container">
        	<input 
          onChange={event => {
            sessionStorage.setItem('eventName',event.target.value);
          }}
          id="event-name" 
          type="text" 
          placeholder="Event Name"/>
        </div>
        <div id="event-details-container">
        	<textarea 
          onChange={event => {
            sessionStorage.setItem('eventDetail',event.target.value);
          }}
          id="event-details" placeholder="Event Details">
         	</textarea>
        </div>
      </div>
    );
  }
}
  