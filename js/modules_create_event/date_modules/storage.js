 import DateTimeCard from "./date_time_card";
 import React from 'react';
 
 export function getData() {
    
    if(sessionStorage.card){
    var card = [];
    var storage = sessionStorage.getItem("card")
    var storageToObj = JSON.parse(storage);
    var dateTime = storageToObj

    for (var i = 0; i < dateTime.length; i++){
      card.push(<DateTimeCard 
      startTime={dateTime[i].start_time}
      endTime={dateTime[i].end_time}
      key={i}
      id={i}
      />)
    }
    
   }
   return card;
 }


   