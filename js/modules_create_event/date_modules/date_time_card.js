import React from "react";

export default class DateTimeCard extends React.Component {
 
  render(){
    return(
      <li className="card" id={this.props.id}>
        <img src="./img/clock.png" alt="timeicon"/>
        <span className="card-span-bold">{this.props.startTime}</span>
        <span className="card-span-bold">{this.props.endTime}</span>
        <button onClick={this.props.click}>
        <img id={this.props.btnId} src="./img/ic_cross.png" alt="close"/>
        </button>
      </li>
   );
  }
}










