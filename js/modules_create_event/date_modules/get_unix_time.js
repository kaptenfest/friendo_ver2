import React from "react";
import {getDate} from "./get_date";
export function getUnixTime() {
      
      var startTime = document.getElementsByClassName('form-control')[0].value;
      var endTime = document.getElementsByClassName('form-control')[1].value;
      var date = new Date();
      var currentTime = date.getTime();
      
      Date.prototype.getUnixTime = function() { return this.getTime() };
      if(!Date.now) Date.now = function() { return new Date(); }
      Date.time = function() { return Date.now().getUnixTime(); }
      
      // Parse a date and get it as Unix time
      startTime = new Date(startTime).getUnixTime();
      endTime = new Date(endTime).getUnixTime();

        if(startTime < currentTime || endTime < currentTime ){
          alert("Start och/eller sluttid har passerat");
        }
        else{
          if(sessionStorage.startTime) {
            let unixStartTime = sessionStorage.getItem("startTime");
                unixStartTime = JSON.parse(unixStartTime);
                unixStartTime.push(startTime);
            sessionStorage.setItem("startTime",JSON.stringify(unixStartTime));
          }
          else {
            let unixStartTime = [];
                unixStartTime.push(startTime);
                sessionStorage.setItem("startTime",JSON.stringify(unixStartTime));
          }
          if(sessionStorage.endTime) {
            let unixEndTime = sessionStorage.getItem("endTime");
                unixEndTime = JSON.parse(unixEndTime);
                unixEndTime.push(endTime);
            sessionStorage.setItem("endTime",JSON.stringify(unixEndTime));
          }
          else {
            let unixEndTime = [];
            unixEndTime.push(endTime);
            sessionStorage.setItem("endTime",JSON.stringify(unixEndTime));
          }
           
        }

        return getDate(startTime, endTime);

    }