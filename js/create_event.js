/*
  *Since I didn't know better at the time sessionStorage instead of redux state is used to store data in create event.
  *React bootstrap carousel is used for event image slider, 
   documentation is available at https://react-bootstrap.github.io/components.html#media-content
*/

import React, {Component} from 'react';
import ReactDOM from "react-dom";
import { Router, Route, IndexRoute, Navigation, hashHistory, IndexLink, Link } from "react-router";
import axios from "axios";
import Details from "./modules_create_event/details";
import Eventdate from "./modules_create_event/eventdate";
import Eventlocation from "./modules_create_event/eventlocation";
import People from "./modules_create_event/people";
import { Carousel, CarouselItem, CarouselCaption } from 'react-bootstrap';

// Hämta user_ID från Localstorage
const user_id = localStorage.getItem("user_id")
  
// Hämta token från Localstorage
const token = localStorage.getItem("token")

export default class AppCreate extends Component{
  constructor(props) {
      super(props);
      this.state = {
        index: 0,
        direction: null,
        eventImages: [],
        selectedImageUrl:"http://app.friendoapp.com/storage/events/social1.jpg"
      };
   }
   /*Select event image*/
   handleSelect(selectedIndex, e) {
    this.setState({
      index: selectedIndex,
      direction: e.direction
    });
    setTimeout(function() {
      var url = document.getElementsByClassName("active")[1].childNodes;
        url = url[0].attributes[4].nodeValue;
       this.setState({selectedImageUrl: url})
    }.bind(this), 1000);
  }

  reload(){
    location.reload();
  }

  componentDidMount() {
     //Sets session storage items
     if(!sessionStorage.eventName) {
        sessionStorage.setItem("eventName",JSON.stringify( ))
     }
     if(!sessionStorage.eventDetail) {
        sessionStorage.setItem("eventDetail",JSON.stringify( ))
     }
      if(!sessionStorage.startTime) {
        sessionStorage.setItem("startTime",JSON.stringify([]))
     }
     if(!sessionStorage.endTime) {
        sessionStorage.setItem("endTime",JSON.stringify([]))
     }
      if(!sessionStorage.time_card) {
        sessionStorage.setItem("time_card",JSON.stringify([]))
     }
      if(!sessionStorage.address) {
        sessionStorage.setItem("address",JSON.stringify([]))
     }
     if(!sessionStorage.lat) {
        sessionStorage.setItem("lat",JSON.stringify([]))
     }
     if(!sessionStorage.lng) {
        sessionStorage.setItem("lng",JSON.stringify([]))
     }
     if(!sessionStorage.people_card) {
       sessionStorage.setItem("people_card",JSON.stringify([]))
     }
     if(!sessionStorage.friendId) {
        sessionStorage.setItem("friendId",JSON.stringify([]))
     }
     if(!sessionStorage.email) {
        sessionStorage.setItem("email",JSON.stringify([]))
     }
      
  const querystring = require('querystring');
  //Fetch friends from api
  axios({
     method: "post",
     headers: {'Content-Type': 'application/x-www-form-urlencoded'},
     url: "/user/list-friends-in-friendo",
     baseURL: "http://app.friendoapp.com/api",

     data: querystring.stringify({
       token: token, 
    }),
  })
  .then(function(response){
    console.log("friends", response)
      var friends = response.data.friends
      var friendsArr = [];
        for (var i = 0; i < friends.length; i++) {
          friendsArr.push({id: friends[i].id, name: friends[i].name})
        }
      var storage = JSON.stringify(friendsArr);
      sessionStorage.setItem("friends",storage);
  });
  //Fetch event images from api
  var self = this;
  axios({
     method: "post",
     headers: {'Content-Type': 'application/x-www-form-urlencoded'},
     url: "/event/get-event-images",
     baseURL: "http://app.friendoapp.com/api",
     data: querystring.stringify({
       token: token, 
    }),
  })

  .then(function(response){
    var arr = [];
    for (var i = 0; i < response.data.images.length; i++){
      arr.push(<Carousel.Item key={i}>
                <img width="550px" height="340px" alt="eventimage" id={i} src={response.data.images[i].url}/>
               </Carousel.Item>)
    }
    self.setState({eventImages: arr})
  });
  
}

//When create event button is clicked
createEvent() {

  var checkInput = sessionStorage.getItem("eventName");
 
  if (checkInput === "undefined" || checkInput.trim() === "") {
    alert("Oops! You have to set a name for the event");
  }
  else {
    var emailArr = JSON.parse(sessionStorage.getItem("email"))
    var friendsArr = JSON.parse(sessionStorage.getItem("friendId")) 
    var selectedImage = this.state.selectedImageUrl.substring(26)
    var eventName = sessionStorage.eventName === "undefined" ? "" : sessionStorage.getItem("eventName")//string
    var eventDetail = sessionStorage.eventDetail === "undefined" ? "" : sessionStorage.getItem("eventDetail")//string
    var startTime = JSON.parse(sessionStorage.getItem("startTime"))//array
    var endTime = JSON.parse(sessionStorage.getItem("endTime"))//array
    var address = JSON.parse(sessionStorage.getItem("address"))//array of objects
    var lat = JSON.parse(sessionStorage.getItem("lat"))//array of numbers
    var lng = JSON.parse(sessionStorage.getItem("lng"))//array of numbers
    var friendId = JSON.parse(sessionStorage.getItem("friendId"))//array of strings
    var eventId;
    var groupId;
    var location = [];
    var location_string = "";
    var invitedFriends = "";
    var time = [];
    var time_string = "";
    //Friends
    if(friendsArr.length > 0){
      //Creates a string of invited friends
      for (var i = 0; i < friendsArr.length; i++) {
        if(friendsArr[i] !== "") {
          if(i < friendsArr.length-1){
            invitedFriends = invitedFriends +(friendsArr[i] + "_off_friendo_0" + ",");
          }
          else {
            invitedFriends = invitedFriends + (friendsArr[i] + "_off_friendo_0");
          }
        }
      }
    }
    //Creates a string of start/endtime(s)
    if(startTime.length > 0) {
      for(var i = 0; i < startTime.length; i++) {
        if(startTime[i] === "") {
          startTime.splice(i,1);
          endTime.splice(i,1);
          i--;
        }
      }
          
      for (var i = 0; i < startTime.length; i++){
      var timeString;
      endTime[i] === null ? endTime[i] = "" : endTime[i] = endTime[i]
      if(startTime.length === 1){
        timeString = "["+"{"+'"id"' + ":" + '"0"'+ "," + '"values"' + ":" + '"' + startTime[i] + '"' + "," + '"endtime"' + ":" + '"' + endTime[i] + '"' + "}" + "]";
      }
      else {
        if(i === 0) {
          timeString = "["+"{"+'"id"' + ":" + '"0"'+ "," + '"values"' + ":" + '"' + startTime[i] + '"' + "," + '"endtime"' + ":" + '"' + endTime[i] + '"' + "}";
        }
        else if(i === startTime.length-1) {
          timeString = "{"+'"id"' + ":" + '"0"'+ "," + '"values"' + ":" + '"' + startTime[i] + '"' + "," + '"endtime"' + ":" + '"' + endTime[i] + '"' + "}" + "]";
        }
        else {
          timeString = "{"+'"id"' + ":" + '"0"'+ "," + '"values"' + ":" + '"' + startTime[i] + '"' + "," + '"endtime"' + ":" + '"' + endTime[i] + '"' + "}";
        }
      }
      time.push(timeString);
    }
    for (var i = 0; i < time.length; i++) {
      
      if (i === 0) {
        time_string = time[i]
      }
      else {
        time_string = time_string + "," + time[i]
      }
    }
   }
   //Creates a string of Location(s)
   if(address.length > 0){
    for(var i = 0; i < address.length; i++) {
      if(address[i] === "") {
        address.splice(i,1);
        lat.splice(i,1);
        lng.splice(i,1);
        i--;
      }
    }
      
    for (var i = 0; i < address.length; i++){
      var locationString;
      if(address.length === 1){
        locationString = "["+"{"+'"values"' + ":" +  '"' + address[i].props.name + '§' + address[i].props.address + '"' + "," + '"latitude"' + ":" + '"' + lat[i] +'"'+ "," + '"longitude"' + ":" + '"'+lng[i]+ '"' + "}" + "]";
      }
      else {
        if(i === 0) {
          locationString = "["+"{"+'"values"' + ":" +  '"' + address[i].props.name + '§' + address[i].props.address + '"' + "," + '"latitude"' + ":" + '"' + lat[i] +'"'+ "," + '"longitude"' + ":" + '"'+lng[i]+ '"' + "}";
        }
        else if(i === address.length-1) {
          locationString = "{"+'"values"' + ":" +  '"' + address[i].props.name + '§' + address[i].props.address + '"' + "," + '"latitude"' + ":" + '"' + lat[i] +'"'+ "," + '"longitude"' + ":" + '"'+lng[i]+ '"' + "}" + "]";
        }
        else {
          locationString = "{"+'"values"' + ":" +  '"' + address[i].props.name + '§' + address[i].props.address + '"' + "," + '"latitude"' + ":" + '"' + lat[i] +'"'+ "," + '"longitude"' + ":" + '"'+lng[i]+ '"' + "}";
        }
      }
      location.push(locationString);
    }
    for (var i = 0; i < location.length; i++) {
      
      if (i === 0) {
        location_string = location[i]
      }
      else {
        location_string = location_string + "," + location[i]
      }
    }
  }
 /*--------api calls--------------*/
  const querystring = require('querystring');
  //Confirmation email
  axios({
   method: "post",
   headers: {'Content-Type': 'application/x-www-form-urlencoded'},
   url: "/event/create-first-step",
   baseURL: "http://app.friendoapp.com/api",

   data: querystring.stringify({
     token: token, 
     name: eventName, //ok
     imageFromServer: selectedImage,
     start_time: time_string,
     end_time: "",
     location: location_string,//ok
     description: eventDetail//ok
   }),
  })
  .then(function(response){
      eventId = response.data.id;
      //Creates new group
      axios({
       method: "post",
       headers: {'Content-Type': 'application/x-www-form-urlencoded'},
       url: "/group/create-new",
       baseURL: "http://app.friendoapp.com/api",

       data: querystring.stringify({
         token: token, 
         access_token: "",
         name: eventName, 
         eventId: eventId,
         invite_users: ""
       }),
      })
      .then(function(response){
          groupId = response.data.groups[0].id
          //Group comment
          axios({
           method: "post",
           headers: {'Content-Type': 'application/x-www-form-urlencoded'},
           url: "/group/comment",
           baseURL: "http://app.friendoapp.com/api",

           data: querystring.stringify({
             token: token, 
             groupid: groupId,
             eventid: eventId,
             callAfterCreateGroup: "yes"
           }),
          })
          .then(function(response){
          //Invite Friends
          axios({
           method: "post",
           headers: {'Content-Type': 'application/x-www-form-urlencoded'},
           url: "/group/editusergroup",
           baseURL: "http://app.friendoapp.com/api",

           data: querystring.stringify({
             token: token, 
             groupid: groupId,
             title: eventName,
             invite_users: invitedFriends
           }),
          })
        });
       });
    });

    //Clear items from session storage
    var storageItems = ["eventName", "eventDetail", "startTime", "endTime", "time_card","address", "lat", "lng", "people_card", "friendId", "email" ]
    for(var i = 0; i < storageItems.length; i++) {
      if(storageItems[i] === "eventName" || storageItems[i] === "eventDetail"){
       sessionStorage.setItem(storageItems[i],JSON.stringify( ))
      }
      else {
       sessionStorage.setItem(storageItems[i],JSON.stringify([]))
      }
    }
    if(document.getElementById("event-name") && document.getElementById("event-details")) {
       document.getElementById("event-name").value="";
       document.getElementById("event-details").value="";
    }
    else if(document.getElementById("date-time-card-list")) {
      let root = document.getElementById("date-time-card-list")
      while( root.firstChild ){
        root.removeChild( root.firstChild );
       }
      }
    else if(document.getElementById("places-card-container")) {
      let root = document.getElementById("places-card-container")
      while( root.firstChild ){
        root.removeChild( root.firstChild );
       }
      }
      else {
      let root = document.getElementById("people-card-container")
      while( root.firstChild ){
        root.removeChild( root.firstChild );
      }
    }
  }
}
render() {
  return (
    <div 
    className="modal-hidden" id="modal" 
    onClick={e => { 
      if(e.target.id === "modal") {
        document.getElementById("modal").className="modal-hidden";
      }
    }}>
    <div id="container">
      <header>
        <h1 id="create-event-header">Create New Event</h1>
        <Link to='/'>
          <button id="btn-close-modal" 
                  onClick={e => document.getElementById("modal").className="modal-hidden"}>
             <img src="img/ic_cross.png"/>
          </button>
        </Link>
      </header>
    <div>
      <Carousel 
        activeIndex={this.state.index} 
        direction={this.state.direction} 
        onSelect={this.handleSelect.bind(this)}>
        {this.state.eventImages}
      </Carousel>
    </div>
    <nav>
     <ul id="navigation">
      <li>
        <Link 
          to="create-event/details" activeClassName="active">Details</Link>
      </li>
      <li>
        <Link to="create-event/eventdate" activeClassName="active">Date/Time</Link>
      </li>
      <li>
        <Link to="create-event/eventlocation" activeClassName="active">Location</Link>
      </li>
      <li>
        <Link to="create-event/people" activeClassName="active">People</Link>
      </li>
     </ul>
   </nav>
      <div className="content">
       {this.props.children}
      </div>
      <div id="footer">
        <Link to='/'>
          <button 
              id="btn-create-event" 
              onClick={this.createEvent.bind(this)}>
              CREATE EVENT
          </button>
        </Link>
      </div>
    </div>
    </div>
  );
 }
}











