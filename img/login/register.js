import React, {Component} from 'react';
import ReactDOM from 'react-dom';

export default class Register extends Component {
	/*handleClick(e){
		if(e.target.id === "btn-login") {
			alert("asdfasdf")
		}
		else {
			var passwordRepeat = document.getElementById("password-repeat").value;
			passwordRepeat = " " ? alert("undefined") : alert("not undefined")
			console.log(passwordRepeat)
		}
	}*/
	render(){
		var emailIcon = require('../img/login/ic_email.png');
		var passwordIcon = require('../img/login/ic_password.png');
		var BackgroundImage = require('../img/login/login.png');

		var bgStyle = {
		    backgroundImage: 'url(' + BackgroundImage + ')',
		    backgroundSize: 'cover',
		    width: '100%',
		    height: '100vh',
		    backgroundPosition: 'center',
		    backgroundRepeat: 'no-repeat'
		}
		var emailInput = {
			backgroundImage: 'url(' + emailIcon + ')',
			paddingLeft: 40,
			backgroundRepeat: 'no-repeat'

		}
		var passwordInput = {
			backgroundImage: 'url(' + passwordIcon + ')',
			paddingLeft: 40,
			backgroundRepeat: 'no-repeat',
		}
		
		return(
			<div style={bgStyle}>
				<div id="login-box">
					<div id="logo-container">
						<img src={require("../img/login/friendo_logo_with_slogan.png")} id="login-logo"/>
					</div>
					<div>
						<form>
							<div id="input-container">
	  					  <input style={emailInput} className="input-field email-input" type="email" name="email" placeholder="e-mail" required />
							  <input style={passwordInput} 
							  			 className="input-field password-input" 
							  			 id="password" 
							  			 title="Minimum 6 characters, one number, one uppercase and one lowercase letter" 
							  			 type="password" 
							  			 name="password" 
							  			 placeholder="password" 
							  			 pattern="(?=^.{6,}$)((?=.*[0-9])|(?=.*W+))(?![.n])(?=.*[A-Z])(?=.*[a-z]).*"
							  			 required 
							  			 />
							  			 <p className="validation01">
  											<span className="invalid">Minimum 6 characters, one number, one uppercase letter and one lowercase letter</span>
  											<span className="valid">Your password meets the requirements.</span>
										</p>
							  <input style={passwordInput} 
							  			 className="input-field password-input" 
							  			 id="password-repeat" 
							  			 title="Minimum 6 characters, one number, one uppercase and one lowercase letter" 
							  			 type="password" 
							  			 name="repeat password" 
							  			 placeholder="repeat password" 
							  			 pattern="(?=^.{6,}$)((?=.*[0-9])|(?=.*W+))(?![.n])(?=.*[A-Z])(?=.*[a-z]).*"
							  			 required 
							  			 />
							  			 <p className="validation01">
  											<span className="invalid">Minimum 6 characters, one number, one uppercase letter and one lowercase letter</span>
  											<span className="valid">Your password meets the requirements</span>
										</p>
						  	</div>
							  <input  className="btn-login" id="btn-register" type="submit" value="Register"/>
							  <input  className="btn-login" id="btn-login" type="submit" value="Login"/>
						</form>
					</div>
				</div>
			</div>
		);
	}
}
ReactDOM.render(<Login />, document.getElementById("app"));