import React, {Component} from 'react';
import { Router, Route, IndexRoute, hashHistory, IndexLink, Link } from "react-router";

export default class Register extends Component {
	constructor(props) {
  	super(props);
  	this.state = {
  		password:"",
  		passwordRepeat:""
  	};
  }
  
  onInputChange(e) {
  	this.setState({passwordRepeat: e.target.value})
  	setTimeout(() => {  
	  	var password = this.state.password;
	  	var passwordRepeat = this.state.passwordRepeat;
	  	var passwordInput = document.getElementById("password-repeat")
	  	var check = true;
	  		for (var i = 0; i < password.length; i++) {
	  			if (password.charAt(i) !== passwordRepeat.charAt(i) || password.length < passwordRepeat.length) {
	  				check = false;
	  			}
	  		}
	  		if(check) {
	  			passwordInput.className = "password-input-repeat-valid"
	  		}
	  		else {
	  			if(passwordInput.className === "password-input-repeat-valid" ){
  					passwordInput.className = "password-input-repeat-invalid"
  				}
	  		}
	  }, 100);
  }
  handleClick() {
  	var email = document.getElementById("emailInput").value;
  	var password = document.getElementById("password").value;
  	var validateEmail = /[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,63}$/;
  	var validatePassword = /(?=^.{6,}$)((?=.*[0-9])|(?=.*W+))(?![.n])(?=.*[A-Z])(?=.*[a-z]).*/;
  	var email = validateEmail.test(email);
  	var password = validatePassword.test(password);
  	
		 console.log(email)
		 console.log(password)
		   
  }
	render(){
		var emailIcon = require('../img/login/ic_email.png');
		var passwordIcon = require('../img/login/ic_password.png');
		var BackgroundImage = require('../img/login/login.png');

		var bgStyle = {
		    backgroundImage: 'url(' + BackgroundImage + ')',
		    backgroundSize: 'cover',
		    width: '100%',
		    height: '100vh',
		    backgroundPosition: 'center',
		    backgroundRepeat: 'no-repeat'
		}
		var emailInput = {
			backgroundImage: 'url(' + emailIcon + ')',
			paddingLeft: 40,
			backgroundRepeat: 'no-repeat'

		}
		var passwordInput = {
			backgroundImage: 'url(' + passwordIcon + ')',
			paddingLeft: 40,
			backgroundRepeat: 'no-repeat',
		}
		
		return(
			<div style={bgStyle}>
				<div id="login-box">
					<div id="logo-container">
						<img src={require("../img/login/friendo_logo_with_slogan.png")} id="login-logo"/>
					</div>
					<div>
				<form>
					<div id="register-container">
					  <input style={emailInput} 
					  			 className="input-field email-input" 
					  			 id="emailInput"
					  			 type="email" 
					  			 name="email" 
					  			 placeholder="e-mail"
					  			 pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,63}$"
					  			 required />
					  			 <p className="validation01">
										<span className="invalid">Please enter a valid email address</span>
										<span className="valid">The email address meets the requirements</span>
									 </p>
					  <input style={passwordInput} 
					  			 className="input-field password-input" 
					  			 id="password"
					  			 onChange ={e => this.setState({password: e.target.value})}
					  			 title="Minimum 6 characters, one number, one uppercase and one lowercase letter" 
					  			 type="password" 
					  			 name="password" 
					  			 placeholder="password" 
					  			 pattern="(?=^.{6,}$)((?=.*[0-9])|(?=.*W+))(?![.n])(?=.*[A-Z])(?=.*[a-z]).*"
					  			 required 
					  			 />
					  			 <p className="validation01">
										<span className="invalid">Minimum 6 characters, one number, one uppercase letter and one lowercase letter</span>
										<span className="valid">Your password meets the requirements.</span>
									 </p>
					  <input style={passwordInput} 
					  			 className="password-input-repeat-invalid" 
					  			 id="password-repeat" 
					  			 onChange={this.onInputChange.bind(this)}
					  			 type="password" 
					  			 name="repeat password" 
					  			 placeholder="repeat password" 
					  			 />
					  			 <p className="validation01">
										<span className="invalid">Passwords do not match</span>
									 </p>
				  	</div>
					  <input  className="btn-login" id="btn-register" type="submit" value="Register" onClick={this.handleClick.bind(this)}/>
				</form>
				<Link to={`/`} ><p className="link login">Login</p></Link>
				</div>
				</div>
			</div>
		);
	}
}
