import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import axios from "axios";
const querystring = require('querystring');

import Register from './register';
import { Router, Route, IndexRoute, redirectTo, browserHistory, hashHistory, IndexLink, Link } from "react-router";
import { login } from "../js/actions/index.js";

export default class Login extends Component {
	componentDidMount(){
  	//Detect browser support for CORS
	if ('withCredentials' in new XMLHttpRequest()) {
	    /* supports cross-domain requests */
	    console.log("CORS supported (XHR)");
	}
	else{
	  if(typeof XDomainRequest !== "undefined"){
	     //Use IE-specific "CORS" code with XDR
	     console.log("CORS supported (XDR)");
	  }
	  else{
	     //Time to retreat with a fallback or polyfill
	     console.loge("No CORS Support!");
  }
}
  }
  login(username, password){
  	// login(a, b)
  	if(password === "" && username === "") {
  		alert("Please enter username and password")
  	}
  	else if(username === "") {
  		alert("Please enter a username")
  	} 
  	else if(password === "") {
  		alert("Please enter a password")
  	}
  	
  	else {
  	axios({
     method: "post",
     headers: {'Content-Type': 'application/x-www-form-urlencoded'},
     url: "/login/web-login",
     baseURL: "http://app.friendoapp.com/api",

     data: querystring.stringify({
       username: username,
       password: password
    }),
  })

  .then(function(response){

	if(response.data.userid !== undefined) {
    	setTimeout(function() {
    	// console.log(response.data.userid)
    	// console.log(response.data.token)
    		localStorage.setItem("user_id", (response.data.userid))
    		localStorage.setItem("token", (response.data.token))
    	}, 200);
			setTimeout(function() {
    		location.reload();
    	}, 300);
	}
	else {
		alert("The email and/or password you have entered is incorrect");
	}
   });
  }
}

	render(){
		var emailIcon = require('../img/login/ic_email.png');
		var passwordIcon = require('../img/login/ic_password.png');
		var BackgroundImage = require('../img/login/login.png');

		var bgStyle = {
		    backgroundImage: 'url(' + BackgroundImage + ')',
		    backgroundSize: 'cover',
		    width: '100%',
		    height: '100vh',
		    backgroundPosition: 'center',
		    backgroundRepeat: 'no-repeat'
		}
		var emailInput = {
			backgroundImage: 'url(' + emailIcon + ')',
			paddingLeft: 40,
			backgroundRepeat: 'no-repeat'

		}
		var passwordInput = {
			backgroundImage: 'url(' + passwordIcon + ')',
			paddingLeft: 40,
			backgroundRepeat: 'no-repeat',
		}
		return(
			<div style={bgStyle} id="background">
				<div id="login-box">
					<div id="logo-container">
						<img src={require("../img/login/friendo_logo_with_slogan.png")} id="login-logo"/>
					</div>
					<div>
				<form>
					<div id="input-container">
					  <input style={emailInput} 
					  		 id="email" className="input-field email-input" 
					  		 type="email" 
					  		 name="email" 
					  		 placeholder="e-mail" required
					  		 onKeyDown={ (e) => {
					  			 	let keyCode = e.keyCode;
					  			 	if(keyCode === 13){
					  			 		this.login(document.getElementById("email").value, document.getElementById("password").value)
					  			 	}
				  			 	}
				  			 }	
				  			 />
					  <input style={passwordInput} 
				  			 className="input-field password-input-login" 
				  			 id="password" 
				  			 type="password" 
				  			 name="password" 
				  			 placeholder="password" 
				  			 onKeyDown={(e) => {
					  			 	let keyCode = e.keyCode;
					  			 	if(keyCode === 13){
					  			 		this.login(document.getElementById("email").value, document.getElementById("password").value)
					  			 	}
				  			 	}				  			 	
				  			 }
				  			/>
		  			</div>
					  <input 
					  	className="btn-login" 
					  	id="btn-register" 
					  	type="button" 
					  	defaultValue="Login" 
					  	onClick={() => this.login(document.getElementById("email").value, document.getElementById("password").value)}/>
				</form>
				<p className="link">Forgot password?</p>
				<Link to="register"><p className="link">Don't have an account yet? Sign up!</p></Link>

			</div>
				</div>
			</div>
		);
	}
}

